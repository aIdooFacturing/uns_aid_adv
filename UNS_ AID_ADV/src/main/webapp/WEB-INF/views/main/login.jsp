
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title" id="myModalLabel"><label>Sign In</label></h1>
      </div>
      <div class="modal-body">
<div class="panel panel-default">
  <div class="panel-body">
  
					 <div class="inner-addon left-addon form-group">
				<span	class="glyphicon inGlyphicon glyphicon-envelope"></span>
					<input type="email" class="form-control" id="inputEmail"
						placeholder="Email">
						</div>
					
					<div class="inner-addon left-addon form-group">
				<span class="glyphicon inGlyphicon glyphicon-lock"></span>
					<input type="password" class="form-control"
						id="inputPassword" placeholder="Password">
				</div>
						<input type="checkbox">Remember	me
			     <p class=errorMsg></p>
      </div><!-- end of panel body -->
</div><!-- end of panel -->

						<a href="#" onclick="register()">Register</a>
						<a href="#" style="float:right">Forgot Password</a>
      </div> <!-- end of modal body -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="signIn()">Sign in</button>
      </div>
    </div> <!-- end of modal content -->
  </div><!-- end of modal dialog -->
  </div><!-- end of modal -->
  <script>
function signIn(){
var userInfo = { 
		user : $("#inputEmail").val(), 
		pw : $("#inputPassword").val()
	};
	if(userInfo.user.length > 0 && userInfo.pw.length > 0){
   $.ajax({
	      url : "${ctxPath}/factory911/submit.do",
	      type : "POST",
	      dataType : "text",
	      contentType : "application/json",
	      data : JSON.stringify(userInfo),
	      success : function(data){
	    	  console.log(data)
	    	  if(data == "success"){	// Login success
	    		 // alert("Login success");
	    		  /* link page*/
	    		  window.location = "${ctxPath}/factory911/chart/main.do";
	    		  
	    	  } else {			// un correct 
	    		  $(".errorMsg").text("The email and password you entered don't match.");
	    	          }
	        },
	      error : function() {
		    alert("connect fail");
	         } 
    });
	}	else {
		$(".errorMsg").text("Enter email and password.");
		}
};
</script>