<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">


<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
</script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script src="${ctxPath }/js/jqMap.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>	
<script type="text/javascript" src="${ctxPath }/js/highstock.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts-3d.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<script src="${ctxPath }/js/card_rail_demo.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/card.css">
</head>

<body >
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	
			
	<div id="panel">
		<table id="panel_table" width="100%">
			<tr align="center">
				<td colspan="2"><p id="comName">MENU</p></td>
			</tr>
			<tr align="center">
				<td width="50%">
					<img alt="" src="${ctxPath }/images/menu/board.png" class="menu_icon" id="DashBoard_1" draggable="false">
					<p>가공 현황</p>
				</td>
				<!-- <td>
					<div  class="menu_icon" id="Report_4" draggable="false" style="background-color: blue"></div>
					<p>블럭가공</p>
				</td> -->
				<td>
					<img alt="" src="${ctxPath }/images/menu/report.png" class="menu_icon" id="Report_3" draggable="false">
					<p>통계</p>
				</td>
			</tr>
		</table>
	</div>
	<div id="corver"></div>
	<img alt="" src="${ctxPath }/images/myApps.png" id="menu_btn">
	
	<!-- Part1 -->
	<div id="part1" class="page" >
		<div id="grid"></div>
		<div class="ghost-select"><span></span></div>
		
		
		<div id="svg">
		</div>
				
		<div id="admin_pwd_box">
			 <center>비밀번호</center>
			 <br>
			<input type="password" id="pwd">
		</div>
		
		<img alt="" src="${ctxPath }/images/logo.png" id="main_logo" style="display: none">
		<table id="time_table">
			<tr>
				<td id="today"></td>
			</tr>
		</table>
		<div id="statusBox">
			<div id="status_pie" style="float: left;"></div>
			<div id="status_pie2" style="float: right;"></div>
		</div>
		<!-- <div id="progressbar"></div> -->
		<div class="container">
			<div id="barChart"></div>
			<!-- <div id="opRatio">68.3%</div> -->
			<table id="main_table" style="border-collapse: collapse; text-align: center; display: none"  >

			</table>
		<canvas id="canvas" width="200" height="200"></canvas>
		<div id="stateBorder"></div>
			<img alt="" src="${ctxPath }/images/map.svg" id="map">
			<center>
				<table class="mainTable" id="mainTable">
					<tr>
						<Td align="center" style="background-color:rgb(34,34,34); font-weight: bolder;" class="title" id="title_main">
								장비 가동 현황
						</Td>
					</tr>
				</table>
				
				<div id="cards">
				
				</div>
			</center>
		</div>
	</div>
	
	<!--Part2  -->
	<div id="part2" class="page">
		<div id="mainTable2">
			<center>
				<table class="mainTable" >
					<tr>
						<Td align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="title" colspan="4">
							<img alt="" src="${ctxPath }/images/loginForm/Factory911_3.png" class="title_left">
								개별 장비 상태
							<img alt="" src="${ctxPath }/images/loginForm/SmartFactory2.png" class="title_right">
						</Td>
					</tr>
					<Tr class="tr1" >
						<td colspan="3" width="60%" rowspan="2" valign="middle">
						<div align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="subTitle">
								Status	
							</div>
							<div id="container"></div>
						</td>
						<td width="40%" valign="top" id="machine_name_td">
							<div align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="subTitle">
								Machine	
							</div>
							<div id="machine_name">
								
							</div>
						</td>		
					</Tr >
					<tr class="tr1">
						<td valign="top">
							<div align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="subTitle">
							Alarm	
							</div>
							<div id="alarm" align="left"></div>
						</td>
						
					</tr>
					<tr class="tr2" valign="top" id="chartTr">
						<td width="20%">
							<div align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="subTitle">
							Operation time Ratio				
							</div>	
							<div class="neon1">
							 	88
							 </div>
						</td>
						<td width="20%">
							<div align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="subTitle">
								Motor End_up Movement		
							</div>
							<div class="neon2">
							04
							</div>
						</td>
						<td width="20%">
							<div align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="subTitle">
							Motor Up_Down Movement				
							</div>
							<div class="neon3" >
							22
							</div>
						</td>
						<Td width="40%">
							<div align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="subTitle">
							Daily Status				
							</div>
							<div id="pie" style="width: 100%"></div>
						</Td>
					</tr>
				</table>
			</center> 
		</div>
	</div>
	
	<!-- Part3 -->
	<div id="part3" class="page" style="background-color: rgb(16, 18, 20)">
		<center>
			<table class="mainTable" id="reportTable"style="background-color: rgb(16, 18, 20)" >
				<tr>
					<Td align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="title"  id="part3_title">
							통계
					</Td>
				</tr>
				<tr >
					<td align="center"  style="background-color:rgb(34,34,34);color:rgb(34,34,34) "class="title part3_no_padding">
						<img alt="" src="${ctxPath }/images/excel.svg" id="excel">
							<div id="reportDateDiv">
								<input type="date" id="sDate"> - 
								<input type="date" id="eDate">
							</div>
					</td>
				</tr>
				<Tr align="center">
					<td style="background-color:rgb(34,34,34); padding: 0" class="title part3_no_padding">
						<table id="tableDiv" style="width: 100%; background-color: rgb(34,34,34); border-collapse: collapse; padding: 0; margin: 0" class="subTable" border="1" >
							<thead>							
								<Tr align="center" >
									<th>
										설비
									</th>
									<th>
										형번
									</th>
									<th>
										길 이(m)
									</th>
									<th>
										설비가동시간 (시간)
									</th>
									<th>
										비가동시간 (시간)
									</th>
									<th>
										시간가동율 (%)
									</th>
									<th>
										사이클타임 (분)
									</th>
									<th>
										현재가동시간 (분)
									</th>
									<th>
										완료사이클
									</th>
									<th>
										생산량
									</th>
									<th>
										대기상황
									</th>
									<th>
										알람내역
									</th>
								</Tr>
							</thead>
							<div class="tbody">
								<Tr>
									<td align="right " width="10%">1호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">2호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">3호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">4호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">5호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">6호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">7호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">8호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">9호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">10호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">11호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">12호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">13호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">14호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">15호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">16호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">17호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">18호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">19호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">20호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">21호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">22호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">23호기</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
								<Tr>
									<td align="right">150T</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
									<td>	</td>
								</Tr>
							</div>								
						</table>
					</td>
				</Tr>
			</table>
		</center>
	</div>
</body>
</html>