var prog_repetition;
var prog_name;
var prog_version;
$(function() {
	setEl();
	getJobList();
	evtBind();
	getMachineInfo();
	gaugeChart();
	getLineChart("lineChart");
});

var line_chart_interval;
var line_chart;
function getLineChart(id){
	Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    $('#' + id).highcharts({
        chart: {
        	backgroundColor : "rgba(0,0,0,0)",
            type: 'spline',
            height : 300,
            width : 550,
            animation: Highcharts.svg, // don't animate in old IE
            marginRight: 10,
            events: {
                load: function () {
                    // set up the updating of the chart each second
                }
            }
        },
        exporting :false,
        credits : false,
        title: {
            text: "Temperature",
            style : {
            	color : "white",
            	fontSize : 20	
            }
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150,
            labels : {
            	style : {
            		color : "white"
            	}
            }
        },
        yAxis: {
            title: {
                text: false
            },
            labels : {
            	style : {
            		color : "white"
            	}
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                    Highcharts.numberFormat(this.y, 2);
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: []
    });
    
    line_chart = $("#" + id).highcharts();
};

var jobArray = new Array();
function getJobList(){
	var url = ctxPath + "/prog/getJobList.do";
	var param  = "dvcId=" + id;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.jobList;
			
			var tr = "";
			$(json).each(function(idx , data){
				var icon = "";
				
//				if(data.condition=="done"){
//					icon = ctxPath + "/images/pass.png";
//				}else if(data.condition=="play"){
//					icon = ctxPath + "/images/gear.png";
//				}else if(data.condition=="pause"){
//					icon = ctxPath + "/images/pauses.png";
//				}else if(data.condition=="stop"){
//					icon = ctxPath + "/images/cancel.png";
//				};
				
				tr += "<tr id='job" + idx + "'>" + 
							"<Td width='5%'>" + (idx+1) + "</td>" +
							"<Td width='30%'>" + data.prgmName + "</td>" +
							"<Td></td>" +
							"<Td width='7%' align='center'><img src='" + icon + "' class='small_icon'></td>" +
						"</tr>";
				
				var array = [data.prgmName, data.targetCnt, data.version];
				jobArray.push(array);
			});
			
			$("#play_list_table").html(tr);
			
			$("#play_list_table tr").click(select_list);
			setEl();
		}
	});
};

var selected_job_list;
function getMachineInfo(){
	var url = ctxPath + "/prog/getSelectedMahcineInfo.do";
	var param = "id=" + id;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var img = "http://factory911.com:8080/emo/machine_images/" + data.pic + ".jpg";
			$("#machine_name").html(data.name);
			$("#icon").attr("src", img);
			//setEl();
		}
	});
};

var barChart;
function evtBind(){
	$("#pdf").click(pdfViewer);
	$("#play_btn").click(clickStartBtn);
	$("#list").click(showList);
	$("#close_btn").click(closePdf);
};

function showList(){
	$("#list").css("display", "none");
	
	if(prog_running && !rotate_gear){
		rotation = setInterval(rotate, 1000);
		rotate_gear = true;
	};
	
	$("#table_article tr").each(function(idx, data){
		$(this).removeClass("selected_list");	
	});
	
	startBtnAble = false;
	
	$("#table_article").animate({
		"left" : 0
	});
	
	$("#machine_info_article").animate({
		"left" : originWidth
	});
	
	$("#play_btn").css({
		"-webkit-filter" : "grayscale(100%)"
	});
};

var isStart = false;
var startBtnAble = false;
var jobId;
function clickStartBtn(){
	first_select = false;
	var play = ctxPath + "/images/play.png";
	var pause = ctxPath + "/images/pause.png";
	
	if(isStart){
		//if(!confirm("Are you sure?")) return;
		$("#play_btn").attr("src", play);
		
		clearInterval(rotation);
		clearInterval(gaugeInterval);
		
		line_worker.postMessage("done");
		progress_worker.postMessage("done");
		
		spdGauge.series[0].points[0].update(0);
		feedGauge.series[0].points[0].update(0);
		
		$("#small_gear").remove();
		prog_running = false;
		rotate_gear = false;
		
		//list에 wait 아이콘 추가
		
		var src;
		if(prog_repetition<c_prog_count){
			src = ctxPath + "/images/pass.png"
		}else{
			src = ctxPath + "/images/wait.png"
		};
		
		var img = document.createElement("img");
		img.setAttribute("src", src);
		img.setAttribute("class", "small_icon");
		
		$(running_prog).children("td:nth(3)").html(img);
	}else{
		running_prog = selected_job_list;
		if(!startBtnAble) return;
		if(!confirm("Did you check tool?")) return;
		$("#play_btn").attr("src", pause);
		$("#widget #progName").html(prog_name);
		
		var img = document.createElement("img");
		img.setAttribute("src", ctxPath + "/images/gear.png");
		img.setAttribute("id", "small_gear");
		
		$(selected_job_list).children("td:nth(3)").html(img);
		
		$(img).css({
			"width" : "50",
		});
		
		rotation = setInterval(rotate, 1000);
		
		prog_running = true;
		rotate_gear = true;
		progress();
		//$("#lineChart").css("opacity",1);
		
		//line_chart.series[0].remove();
		
		getGaugeData();
		getLineData();
		
		jobId = running_prog.id.substr(3);
		
		window.sessionStorage.setItem("jobId", jobId);
		window.sessionStorage.setItem("currentCnt", 1);
		
		
		$("#count").html("Count : " + c_prog_count + "/" + prog_repetition);
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));

		$("#startTime").html("Start Time<br>-" + hour + ":" + minute + ":" + second);
		
		//estimated_time
		//totalTime
		
		date.setSeconds(date.getSeconds() + totalTime);
		hour = addZero(String(date.getHours()));
		minute = addZero(String(date.getMinutes()));
		second = addZero(String(date.getSeconds()));
		
		$("#endTime").html("Estimated Time Of Completion<br>-" + hour + ":" + minute + ":" + second);
	};
	
	isStart = !isStart;
};

function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

var spdGauge, feedGauge;
function gaugeChart(){
	 var gaugeOptions = {

		        chart: {
		            type: 'solidgauge',
		            backgroundColor : "rgba(0,0,0,0)",
		        },

		        title: null,

		        pane: {
		            center: ['50%', '60%'],
		            size: '100%',
		            startAngle: -90,
		            endAngle: 90,
		            background: {
		                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
		                innerRadius: '60%',
		                outerRadius: '100%',
		                shape: 'arc'
		            }
		        },
		        credits : false,
		        exporting : false,
		        tooltip: {
		            enabled: false
		        },

		        // the value axis
		        yAxis: {
		            stops: [
		                [0.1, '#55BF3B'], // green
		                [0.5, '#DDDF0D'], // yellow
		                [0.9, '#DF5353'] // red
		            ],
		            lineWidth: 0,
		            minorTickInterval: null,
		            tickPixelInterval: 400,
		            tickWidth: 0,
		            title: {
		                y: -60
		            },
		            labels: {
		                y: 156
		            }
		        },

		        plotOptions: {
		            solidgauge: {
		                dataLabels: {
		                    y: 50,
		                    borderWidth: 0,
		                    useHTML: true
		                }
		            }
		        }
		    };

		    // spdLoad
		    $('#spdLoad').highcharts(Highcharts.merge(gaugeOptions, {
		        yAxis: {
		            min: 0,
		            max: 150,
		            labels : {
		            	enabled : false
		            },
		            title: {
		                text: 'Spindle Load',
		                style : {
		                	color : "white",
		                	fontSize : 20
		                }
		            }
		        },

		        credits: {
		            enabled: false
		        },

		        series: [{
		            name: 'Speed',
		            data: [0],
		            dataLabels: {
		                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
		                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'white') + '">{y}</span><br/>' +
		                       '<span style="font-size:12px;color:silver"></span></div>'
		            },
		            tooltip: {
		                valueSuffix: ' km/h'
		            }
		        }]

		    }));

		    // feedoverride
		    $('#feedOverride').highcharts(Highcharts.merge(gaugeOptions, {
		        yAxis: {
		            min: 0,
		            max: 150,
		            labels : {
		            	enabled : false
		            },
		            title: {
		                text: 'Feed Override',
		                style : {
		                	color : "white",
		                	fontSize : 20
		                }
		            },
		        },

		        series: [{
		            name: 'RPM',
		            data: [0],
		            dataLabels: {
		                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
		                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'white') + '">{y}</span><br/>' +
		                       '<span style="font-size:12px;color:silver"></span></div>'
		            },
//		            tooltip: {
//		                valueSuffix: ' revolutions/min'
//		            }
		        }]

		    }));

		    spdGauge = $("#spdLoad").highcharts();
		    feedGauge = $("#feedOverride").highcharts();
};

function getLineData(){
	if(!line_chart.series[0]){
		 var data = [],
         time = (new Date()).getTime(),
         i = 0;

		 for (i = -10; i <= 0; i += 1) {
             data.push({
                 x: time + i * 1000,
                 y: 20 + (Math.random())
             });
         }
             
		line_chart.addSeries({name:"line",data:data,"color" : "#71C9EB"});
		//line_chart.series[0].data.length=5
	};
		
	 var series = line_chart.series[0];
     
     line_worker.postMessage("start");
     line_worker.onmessage = function(evt){
    	 series.addPoint(evt.data, true, true);
     };
};

var line_worker = new Worker(ctxPath + "/js/lineChartWorker.js");

function callLineWorker(){
	line_worker.postMessage("start");
	var series = line_chart.series[0];
	
	line_worker.onmessage = function(evt){
		
	};
};

var gaugeInterval = null;
function getGaugeData(){
	gaugeInterval = setInterval(function () {
        // spd
        var spdPoint = spdGauge.series[0].points[0];
        var spdVal = 70 + Math.floor(Math.random()*10) + Math.floor(Math.random()*10);
        spdPoint.update(spdVal);

        // feed
        var feedPoint = feedGauge.series[0].points[0];
        var feedVal = 70 + Math.floor(Math.random()*10) + Math.floor(Math.random()*10);
        feedPoint.update(feedVal);
    }, 2000);
};

var totalTime = 60;
var progressBar = 0;
var c_prog_count = 1;
var prog_running = false;
var running_prog;
var rotate_gear = false;
var target;
function progress(){
	var play = ctxPath + "/images/play.png";
	var pause = ctxPath + "/images/pause.png";
	target = $(".barContainer").width();
	var speed = target/totalTime/10;
	
	if(prog_running) callProgressWorker(speed);
};

var progress_worker = new Worker(ctxPath + "/js/progressWorker.js");
function callProgressWorker(speed){
	if(!!window.Worker){
		progress_worker.onmessage = function(evt){
			for(var i = 1 ; i < c_prog_count; i++){
				$("#bar" + i).css("width", target);
			};
			
			$("#bar" + c_prog_count).css("width", progressBar+=Number(evt.data));
			
			if(Number(progressBar)>=target){
				addCount();
				progressBar = 0;
				c_prog_count ++;
				//clearInterval(line_chart_interval);
				progress_worker.postMessage("done");
				line_worker.postMessage("done");
				//worker.terminate();
				clickStartBtn();
				window.sessionStorage.setItem("currentCnt", c_prog_count);
			};
		};
		progress_worker.postMessage(speed);
	}else{

	};
};

function addCount(){
	
};

function rotate(){
	$("#gear, #small_gear").rotate({
		angle:0,
	    animateTo:360,
	    //callback: rotate,
	    easing: function (x,t,b,c,d){        // t: current time, b: begInnIng value, c: change In value, d: duration
	      return c*(t/d)+b;
	    }
	});
};

var rotation = null;

function closePdf(){
	$("iframe").attr("src", "");
	$("iframe").css({
		"z-index" : -1,
	});
	$("#close_btn").css("display","none");
	$("#loader").css("display", "none");
}

function pdfViewer(){
	var src = "http://docs.google.com/gview?url=http://106.240.234.114:8080/emo/sample.pdf&embedded=true";
	$("#loader").css("display", "inline");
	$("iframe").attr("src", src);
	$("iframe").css({
		"z-index" : 10,
	});
	$("#close_btn").css("display","inline");
}


function select_list(){
	//prgmName, targetCnt, progVersion
	
	$("#list").css("display", "inline");
	
	var job = jobArray[this.id.substr(3)];
	prog_name = job[0];
	prog_repetition = job[1];
	prog_version = job[2];
	
	$("#machine_info_article .progName").html(prog_name);
	$("#machine_info_article .progVersion").html("v " + prog_version).css("font-size", getElSize(50));
	
	if(running_prog != this) {
		clearInterval(rotation);
		clearInterval(line_chart_interval);
		rotate_gear = false;
		//drawBar();
		//c_prog_count = 1;
		//progressBar = 0;
		
		//$("#lineChart").css("opacity",0);
		
		drawBar();
		//$("#chkBoxTd").css("opacity",0);
		
		line_worker.postMessage("done");
		clearInterval(gaugeInterval);
		spdGauge.series[0].points[0].update(0);
		feedGauge.series[0].points[0].update(0);
	}else{
	//	$("#lineChart").css("opacity",1);
		$("#chkBoxTd").css("opacity",1);
	};

	if(prog_running && running_prog == this){
		getGaugeData();
		getLineData();
		drawBar();
	}else if(prog_running && running_prog != this){
		if(line_chart.series[0]) line_chart.series[0].remove(true);
	};
	
	selected_job_list = this;
	startBtnAble = true;
	$(this).addClass("selected_list");
	
	$("#table_article").animate({
		"left" : -originWidth
	});
	
	$("#machine_info_article").animate({
		"left" : 0
	});
	
	$("#play_btn").css({
		"-webkit-filter" : "grayscale(0%)"
	});
	
	//drawBarChart("barChart");
	//if(!prog_running && running_prog != this){
	if(first_select){
		$("#chkBoxTd").css("opacity",1);
		$(".squaredTwo").remove();
		drawBar();
	};
};

var first_select = true;
function drawBar(){
	var bar = "";
	
	for(var i = 0; i < prog_repetition; i++){
		bar += "<div class='barContainer'><div class='bar' id='bar" + (i+1) + "' style='font-size:15; font-weight:bolder'><span class='count_span'>" + (i+1) + "</span></div></div>";
	};
	
	$("#barChart").html(bar);
	
	$(".barContainer").css({
		"border-left" : "1px solid white",
		"border-right" : "1px solid white",
		"width" : 300,
		"height" : 20,
		"margin-top" : 20
	});
	
	$(".bar").css({
		"background-color" : "rgb(114,191,60)",
		"width" : 0,
		"height" : 20,
	});
	
	$(".count_span").css({
		"position" : "absolute",
		"left" : 580
	});
	
	var chkBox = "";
	for(var i = 0; i < prog_repetition; i++){
		chkBox += "<div class='chkBox' id='chkBox" + (i+1) + "'>" + 
						"<input type='checkBox'  id='checkBox" + (i+1) + "' class='css-checkbox'>" + 
						"<label for='checkBox" + (i+1) + "' class='css-label'></label>" + 
					"</div>";
	};
	
	$(".chkBox").remove();
	$("#chkBoxTd").append(chkBox);
	
	var margin = 40;
	$(".chkBox").each(function(idx, data){
		$("#chkBox" + (idx+1)).css({
			"position" : "absolute",
			"right" : 20,
			//"top" : margin * idx + 45
			"top" : $("#bar" + (idx+1)).offset().top - $("#machine_info_article").offset().top
		});
	});
};

function drawBarChart(id){
	 $('#' + id).highcharts({
		 	credits : false,
		 	exporting : false,
	        chart: {
	        	width : 300,
	        	margin : 0,
	        	backgroundColor : "rgba(0,0,0,0)",
	            type: 'bar'
	        },
	        title: {
	            text: false
	        },
	        xAxis: {
	        	gridLineWidth: 0,
	        	labels : {
	        		enabled : false
	        	},
	            categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
	        },
	        yAxis: {
	        	gridLineWidth: 0,
	        	labels : {
	        		enabled : false
	        	},
	            min : 0,
	            max : 5,
	            title: {
	                text: 'Total fruit consumption'
	            }
	        },
	        legend: {
	        	enabled : false
	        },
	        plotOptions: {
	            series: {
	                stacking: 'normal'
	            }
	        },
	        tooltip : {
	        	enabled :false
	        },
	        series: [{
	        	color : "#73F565",
	            data: [5,5,5,5,5]
	        }]
	    });
	 
	 barChart = $("#" + id).highcharts();
};

function setEl() {
	$("#play_list_table tr").addClass("list");
	
	$("iframe").css({
		"position" : "absolute",
		"width" : "90%",
		"height" : "90%",
		"z-index" : -1
	});
	
	$("iframe").css({
		"left" : (originWidth/2) - ($("iframe").width()/2),
		"top" : (originHeight/2) - ($("iframe").height()/2)
	});
	
	$("#close_btn").css({
		"position" : "absolute",
		"background-color" : "white",
		"border-radius" : "50%",
		"width" : "5%",
		"display" : "none",
		"z-index" : 11
	});
	
	$("#icon_td").css({
		"height" : originHeight * 0.3
	});
	
	$("#close_btn").css({
		"left" : $("iframe").offset().left - ($("#close_btn").width()/2),
		"top" : $("iframe").offset().top - ($("#close_btn").height()/2)
	});
	
	$("#loader").css({
		"position" : "absolute",
		"left" : (originWidth/2) - ($("#loader").width()/2),
		"top" : (originHeight/2) - ($("#loader").height()/2),
		"z-index" : 9,
		"display" : "none"
 	});
	
	$("#widget").css({
		"bottom" : 0,
	});
	
	$("#table_article").css({
		"width" : "100%",
		"position" : "absolute",
		"top" : $("#title_table").offset().top + $("#title_table").height(),
	});
	
	$("#table_article").css({
		"height" : $("#widget").offset().top - $("#table_article").offset().top
	});
	
	$("#machine_info_article").css({
		"position" : "absolute",
		"top" : $("#table_article").offset().top,
		"height" : $("#table_article").height(),
		"left" : originWidth,
	});
	
	$("#list").css({
		"position" : "absolute",
		"height" : $("#widget").height() * 0.8,
		"left" : 30,
		"display" : "none"
	});

	$("#list").css({
		"top" : ($("#widget").height()/2) - ($("#list").height()/2),
	});
	
	$("hr").css({
		"color" : "blue",
		"margin-top" : 30,
		"border" : "1px solid blue"
	});
	
	$("#chkBox1").css({
		"position" : "absolute",
		"right" : 20,
		"top" : 40
	});
	
	$("#chkBox2").css({
		"position" : "absolute",
		"right" : 20,
		"top" : 110
	});
	
	$("#chkBox3").css({
		"position" : "absolute",
		"right" : 20,
		"top" : 180
	});
	
	$("#chkBox4").css({
		"position" : "absolute",
		"right" : 20,
		"top" : 250
	});
	
	$("#chkBox5").css({
		"position" : "absolute",
		"right" : 20,
		"top" : 320
	});
};