package com.unomic.dulink.program.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.program.domain.ProgramVo;


@Service
@Repository
public class ProgramServiceImpl implements ProgramService{
	private final static String namespace= "com.unomic.dulink.prog.";

	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;
	
	@Override
	public ChartVo getSelectedMahcineInfo(ChartVo chartVo) throws Exception {
		
		chartVo = (ChartVo) sql_ma.selectOne(namespace + "getSelectedMahcineInfo", chartVo);
		return chartVo;
	}

	@Override
	public String getJobList(ProgramVo progVo) throws Exception {
		List <ProgramVo> jobList = sql_ma.selectList(namespace + "getJobList", progVo);
		
		List list = new ArrayList();
		for(int i = 0; i < jobList.size(); i++){
			Map map = new HashMap();
			map.put("prgmId", jobList.get(i).getPrgmId());
			map.put("dvcId", jobList.get(i).getDvcId());
			map.put("prgmName", jobList.get(i).getName());
			map.put("targetCnt", jobList.get(i).getTargetCnt());
			map.put("version", jobList.get(i).getVersion());
			map.put("condition", jobList.get(i).getCondition());
			
			list.add(map);
		};
		
		String str = "";
		Map jobMap = new HashMap();
		jobMap.put("jobList", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(jobMap);
		return str;
	}

};