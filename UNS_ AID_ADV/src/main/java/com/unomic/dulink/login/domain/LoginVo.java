package com.unomic.dulink.login.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LoginVo {
	String email;
	String pwd;
	String NC;
	int userId;
	int NCId;
	String company;
};
