package com.unomic.dulink.svg.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.svg.domain.SVGVo;


@Service
@Repository
public class SVGServiceImpl implements SVGService{
	private final static String namespace= "com.unomic.smarti.svg.";
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;
	
	@Override
	public String getMachineInfo(SVGVo svgVo) throws Exception {

		List <SVGVo> machineList = sql_ma.selectList(namespace + "getMachineInfo");
		List list=new ArrayList();
		
		for(int i = 0; i < machineList.size(); i++){
			Map map = new HashMap();
			map.put("id", ((SVGVo) machineList.get(i)).getId());
			map.put("dvcId", ((SVGVo) machineList.get(i)).getDvcId());
			map.put("name", ((SVGVo) machineList.get(i)).getName());
			map.put("x", ((SVGVo) machineList.get(i)).getX());
			map.put("y", ((SVGVo) machineList.get(i)).getY());
			map.put("w", ((SVGVo) machineList.get(i)).getW());
			map.put("h", ((SVGVo) machineList.get(i)).getH());
			map.put("pic", ((SVGVo) machineList.get(i)).getPic());
			map.put("lastChartStatus", ((SVGVo) machineList.get(i)).getLastChartStatus());
			map.put("operationTime", ((SVGVo) machineList.get(i)).getOperationTime());
			map.put("ieX", machineList.get(i).getIeX());
			map.put("ieY", machineList.get(i).getIeY());
			
			list.add(map);
		};
		
		String str="";
		
		Map resultMap=new HashMap();
		resultMap.put("machineList", list);
		
		ObjectMapper om = new ObjectMapper();
		str=om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		
		return str;
	}

	@Override
	public void setMachinePos(SVGVo svgVo) throws Exception {
		
		sql_ma.update(namespace + "setMachinePos", svgVo);
	}

	@Override
	public SVGVo getMarker() throws Exception {
		
		
		SVGVo svgVo = new SVGVo();
		svgVo = (SVGVo) sql_ma.selectOne(namespace + "getMarker"); 
		return svgVo;
	}

	@Override
	public String getMachineInfo2(SVGVo svgVo) throws Exception {
		List machineList = sql_ma.selectList(namespace + "getMachineInfo2");
		List list=new ArrayList();
		
		for(int i = 0; i < machineList.size(); i++){
			Map map = new HashMap();
			map.put("id", ((SVGVo) machineList.get(i)).getId());
			map.put("name", ((SVGVo) machineList.get(i)).getName());
			map.put("x", ((SVGVo) machineList.get(i)).getX());
			map.put("y", ((SVGVo) machineList.get(i)).getY());
			map.put("w", ((SVGVo) machineList.get(i)).getW());
			map.put("h", ((SVGVo) machineList.get(i)).getH());
			map.put("pic", ((SVGVo) machineList.get(i)).getPic());
			map.put("lastChartStatus", ((SVGVo) machineList.get(i)).getLastChartStatus());
			map.put("operationTime", ((SVGVo) machineList.get(i)).getOperationTime());
			map.put("dvcId", ((SVGVo) machineList.get(i)).getDvcId());
			
			list.add(map);
		};
		
		String str="";
		
		Map resultMap=new HashMap();
		resultMap.put("machineList", list);
		
		ObjectMapper om = new ObjectMapper();
		str=om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		
		return str;
	}

	@Override
	public void setMachinePos2(SVGVo svgVo) throws Exception {
		sql_ma.update(namespace + "setMachinePos2", svgVo);
	}

	@Override
	public SVGVo getMarker2() throws Exception {
		SVGVo svgVo = new SVGVo();
		svgVo = (SVGVo) sql_ma.selectOne(namespace + "getMarker2"); 
		return svgVo;
	};
	
};

