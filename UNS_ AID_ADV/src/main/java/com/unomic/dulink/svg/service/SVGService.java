package com.unomic.dulink.svg.service;

import java.util.Map;

import com.unomic.dulink.svg.domain.SVGVo;

public interface SVGService {
	public String getMachineInfo(SVGVo svgVo) throws Exception;
	public void setMachinePos(SVGVo svgVo) throws Exception;
	public SVGVo getMarker() throws Exception;
	
	public String getMachineInfo2(SVGVo svgVo) throws Exception;
	public void setMachinePos2(SVGVo svgVo) throws Exception;
	public SVGVo getMarker2() throws Exception;
};
