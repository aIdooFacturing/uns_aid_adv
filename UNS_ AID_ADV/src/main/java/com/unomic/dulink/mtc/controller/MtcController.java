package com.unomic.dulink.mtc.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.mtc.domain.MTConnect;
import com.unomic.dulink.mtc.domain.MtcCtrlVo;
import com.unomic.dulink.mtc.domain.RogerParser;
import com.unomic.dulink.mtc.service.MtcService;
import com.unomic.factory911.adapter.domain.AdapterVo;
import com.unomic.factory911.adapter.service.AdapterService;
import com.unomic.factory911.device.domain.DeviceStatusVo;
import com.unomic.factory911.device.domain.DeviceVo;
import com.unomic.factory911.device.service.DeviceService;
import com.unomic.factory911.mc_prgm.domain.McPrgmVo;
import com.unomic.factory911.mc_prgm.service.McPrgmService;

/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/mtc")
@Controller
public class MtcController {
	
	private static final Logger logger = LoggerFactory.getLogger(MtcController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	@Autowired
	private DeviceService deviceService;
	
	@Autowired
	private MtcService mtcService;

	@Autowired
	private AdapterService adapterService;

	@Autowired
	private McPrgmService mcPrgmService;
	
	@RequestMapping(value = "getInitTime")
	@ResponseBody
    public String setInitTime(){
		return CommonFunction.getTodayDateTime();
	}
	
	@RequestMapping(value = "timeTest")
	@ResponseBody
    public String timeTest(){
		return CommonFunction.getTodayDateTime();
	}
	
	@RequestMapping(value = "ver")
	@ResponseBody
    public String getVer(){
			
		return "v1.0001";
	}
	
	
	@RequestMapping(value = "dbTest")
	@ResponseBody
	public String dbTest(HttpServletRequest request){
		AdapterVo inputVo = new AdapterVo();
		inputVo.setDvcId("1");
		AdapterVo preVo = adapterService.getLastInputData(inputVo);
		logger.info("preVo:"+preVo);
		return "OK";
	}
	
	@RequestMapping(value = "dbTest2")
	@ResponseBody
	public String dbTest2(HttpServletRequest request, AdapterVo inputVo){
		logger.info("inputVo:"+inputVo);

		adapterService.testDatetime2(inputVo);
		
		return "OK";
	}
	
	/*
	 * 	#{dvcId} : 장비 ID 
	 * 	#{chgTy} : 컨트롤 데이터 변경 종류. S/F/D/H 중 1개.
	 * 	#{chgDt} : 변경 신호받은 datetime yyyy-MM-dd HH:Mi:ss
	 * */
	@RequestMapping(value = "ctrlChg")
	@ResponseBody
	public String ctrlChg(HttpServletRequest request, MtcCtrlVo inputVo){
		
		String rtnStr = mtcService.setChgDvcCtrl(inputVo);
		
		return rtnStr;
	}
	
	@RequestMapping(value = "mtcCrnt")
	@ResponseBody
    public String mtcCrnt(@RequestBody String mtcCrnt, HttpServletRequest request){
		
		try {
			mtcCrnt = mtcCrnt.replaceAll("%(?![0-9a-fA-F]{2})", "%25"); 
			mtcCrnt = URLDecoder.decode(mtcCrnt, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			logger.error("UTF DECODING FAILED");
			return "UTF_DECODE_FAIL.";
		}
		mtcCrnt = mtcCrnt.trim();
//		logger.info("mtcCrnt : "+mtcCrnt);
//		logger.info("into Crnt case");
		if(mtcCrnt==null){
			logger.error("return null case : no xml Data 0");
			return "XML data is null.";
		}
		if(mtcCrnt.length() < 1){
			logger.error("length < 1 : no xml Data 1["+request.getRemoteAddr()+"]["+mtcCrnt+"]");
			return "no xml Data1";
		}
		String[] result = mtcCrnt.split(" ");
		
//Query 1
		if("DUPLE".equals(result[0])){
			logger.info(result[1]);

			//cannon
			//return deviceService.editDvcLastTime(result[1]);
			logger.error("DUPLE:"+result[1]);
			return deviceService.addDvcDuple(result[1]);
			//return "OK";
			
		}
		
		//mtcCrnt = mtcCrnt.replaceAll("[^\\x20-\\x7e]", "");
		List<String> listXML = getListFromInputXML(mtcCrnt);
		
		//logger.info("listXML:"+listXML);
		// mtc xml list 가 들어가서 pureStatusVo 가 나와서 일괄 처리한다.

		MTConnect mtConnect = new MTConnect();
		
		//1차 리스트 생성. 가공된 차트 상태 값까지 들어감.
		List<AdapterVo> listAdt= new ArrayList<AdapterVo>();

		// First loop
		// set chart status.
		
		logger.info("size Xml:"+listXML.size());
		
		for(int i = 0, size=listXML.size(); i < size ; i++){
			String tmpStr = listXML.get(i);
			
			mtConnect = getMTCfromXML(tmpStr);
			if(mtConnect==null){
				return "XML_FORMAT_ERROR";
			}
			if(mtConnect.getStatus() == null
				|| mtConnect.getStatus().equals(CommonCode.MSG_UNAVAIL)
				|| mtConnect.getSpdLd().equals("NaN")
				) {
				//이 조건문에 부합할 경우 데이터 가공이 안되므로 리턴시킨다.
				return "Status_IS_UNAVAILABLE:"+mtcCrnt;
			}

			AdapterVo tmpVo = getAGTStatus(mtConnect);
	
			logger.info("tmpVo.getSeq():"+tmpVo.getSeq());
			logger.info("tmpVo.getIoLogik():"+tmpVo.getIoLogik());
			logger.info("getMainPrgmStartTime:"+tmpVo.getMainPrgmStartTime());
			logger.info("getMainPrgmStartTime2:"+tmpVo.getMainPrgmStartTime2());
			logger.info("tmpVo.getSender():"+tmpVo.getSender());
			logger.info("tmpVo.getAlarmMsg1():"+tmpVo.getAlarmMsg1());
			logger.info("tmpVo.getAlarmMsg2():"+tmpVo.getAlarmMsg2());
			logger.info("tmpVo.getAlarmMsg3():"+tmpVo.getAlarmMsg3());
			logger.info("tmpVo.getIsZeroActSpd():"+tmpVo.getIsZeroActSpd());
			logger.info("tmpVo.getIsZeroSpdLoad():"+tmpVo.getIsZeroSpdLoad());
			listAdt.add(tmpVo);
		}

		List<McPrgmVo> listMcPrgm = null;
		for(AdapterVo i: listAdt){
			i = setAgtChartStatus(i,listMcPrgm);
		}

		logger.info("size:"+listAdt.size());
		
		AdapterVo lastVo = listAdt.get(listAdt.size()-1);
		lastVo.setLastIpAddr(request.getRemoteAddr());

		//Query 2
		// dateStarter를 위해 예전 데이터 가져옴.
		// 현재 중복 제거는 안함
		
		AdapterVo preVo = new AdapterVo();
		try{
			preVo = adapterService.getLastInputData(lastVo);
		}catch(Exception e){
			logger.error("Case Query 2");
			//logger.error(e.getMessage())
		}
		
		logger.info("preVo:"+preVo);
		preVo = setIsZeroStatus(preVo);

		// 리스트 가져오는데까지 확인.
		// 값 넣고 상태값 비교하는데까지 하면 1차 목표 완성.(완료)		
		// 2차 목표는 형번 포함된 사이클 갯수, 생산 갯수, 길이. 
		// 형번 포함된 가공 정보 처리가 고민.
		// Second Loop
		// dateStarterSetter
		
		for(int i =0, size = listAdt.size() ; i < size ; i++){
			AdapterVo tmpVo = listAdt.get(i);
			AdapterVo tmpPreVo;
			AdapterVo dateStarterVo;
			if(i < 1){
				tmpPreVo = preVo;
			}else{
				tmpPreVo = listAdt.get(i - 1);
			}
			logger.info("i:"+i);
			logger.info("tmpPreVo:"+tmpPreVo.getStartDateTime());
			logger.info("tmpVo:"+tmpVo.getStartDateTime());
			dateStarterVo = getDateStarter(tmpPreVo, tmpVo);
			logger.info("dateStarterVo:" +( (dateStarterVo == null)?"null" : dateStarterVo.toString() )) ;
			if(dateStarterVo != null){
				listAdt.add(i, dateStarterVo);
			}
		}
		
		//End time Setter
		for(int i = 0, size = listAdt.size() ; i < size ; i++){
			AdapterVo tmpVo = listAdt.get(i);
			AdapterVo tmpPreVo;

			if(i > 0){
				tmpPreVo = listAdt.get(i - 1);
				tmpPreVo.setEndDateTime(tmpVo.getStartDateTime());
				listAdt.set(i-1, tmpPreVo);
			}
		}
		
		List <DeviceStatusVo> dvcList = new ArrayList <DeviceStatusVo> ();
		
		//int size = listAdt.size();
		for(int i = 0, size = listAdt.size() ; i < size ; i++){
			AdapterVo tmpVo = listAdt.get(i);
			DeviceStatusVo crtDvcVo = new DeviceStatusVo();
			
			crtDvcVo.setDvcId(lastVo.getDvcId());
			crtDvcVo.setStartDateTime(tmpVo.getStartDateTime());
			crtDvcVo.setChartStatus(tmpVo.getChartStatus());
			
			crtDvcVo.setSpdLd(tmpVo.getSpdLd());
			crtDvcVo.setSpdOvrd(tmpVo.getSpdOvrd());
			
			crtDvcVo.setAlarmNum1(tmpVo.getAlarmNum1());
			crtDvcVo.setAlarmNum2(tmpVo.getAlarmNum2());
			crtDvcVo.setAlarmNum3(tmpVo.getAlarmNum3());

			crtDvcVo.setAlarmMsg1(tmpVo.getAlarmMsg1());
			crtDvcVo.setAlarmMsg2(tmpVo.getAlarmMsg2());
			crtDvcVo.setAlarmMsg3(tmpVo.getAlarmMsg3());

			// DateStarter면 별작업 없이 넘김.
			if(crtDvcVo.getChartStatus().equals(CommonCode.MSG_DATE_START))
			{continue;}
			
			if(dvcList.size() == 0 ){
				dvcList.add(crtDvcVo);
			}else{
				//여기서 할 작업. 이전 데이터와 비교해서 chartStatus 다르면 list에 넣고 아니면 넣을 필요 없음.
				if(crtDvcVo.getChartStatus().equals(dvcList.get(dvcList.size()-1).getChartStatus())	){
					continue;
				}else{
					dvcList.add(crtDvcVo);
				}
			}
		}

		String strRtn="";

		try {
//Query 3
			strRtn = adapterService.editLastNAddList(listAdt , dvcList);
		} catch (DuplicateKeyException e) {
			// TODO: handle exception
			//e.printStackTrace();
			logger.error("DUPLE:"+lastVo.getSender());
			strRtn = "DUPLE";
		}
		logger.error("OK:"+lastVo.getSender());
		return strRtn ;
	}
	
	private AdapterVo getDateStarter(AdapterVo preVo, AdapterVo crtVo){
		
		if(CommonFunction.isDateStart(preVo.getStartDateTime(), crtVo.getStartDateTime())){
			AdapterVo starterVo = new AdapterVo();
			starterVo.setDvcId(crtVo.getDvcId());
			starterVo.setStartDateTime(CommonFunction.getStandardHourToday());
			starterVo.setChartStatus(CommonCode.MSG_DATE_START);
			starterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));
			starterVo.setSender(crtVo.getSender());
			//최근 데이터 endTime edit.
			//editLastEndTime(starterVo);
			//DateStarter input.
			//addPureStatus(starterVo);
			return starterVo;
		}else{
			return null;
		}
	}
	
	private AdapterVo getDateStarterUTC_(AdapterVo preVo, AdapterVo crtVo){
		if(CommonFunction.isDateStartUTC_(preVo.getStartDateTime(), crtVo.getStartDateTime())){
			AdapterVo starterVo = new AdapterVo();
			starterVo.setDvcId(crtVo.getDvcId());
			starterVo.setStartDateTime(CommonFunction.getStdHourTodayUtc_());
			starterVo.setChartStatus(CommonCode.MSG_DATE_START);
			starterVo.setWorkDate(CommonFunction.utcIsoDt2UtcWorkDate_(CommonFunction.getStdHourTodayUtc_()));
			starterVo.setSender(crtVo.getSender());
			return starterVo;
		}else{
			return null;
		}
	}
	
	// 
	//private List<String> getListInputList(String inputXml){
	private List<String> getListFromInputXML(String inputXml){
		
		Scanner sc = new Scanner(inputXml).useDelimiter(CommonCode.MSG_XML_DELIMITER);
		
		ArrayList<String> listXml = new ArrayList<String>();
		
		while(sc.hasNext()){
			String tmp = sc.next()+CommonCode.MSG_XML_DELIMITER;
			logger.info("tmp:"+ tmp);
			listXml.add(tmp.trim());
		}
		
		//Iterator<String> it = listXml.iterator();
		//while(it.hasNext()){logger.info("xml:[" + it.next() + "]");}
//		logger.info("listXml.size():"+listXml.size());
//		logger.info("lastListXml.:"+listXml.get(listXml.size()-1));	
//		listXml.remove(listXml.size()-1);
		
		return listXml;
	}
	
	private AdapterVo getAGTStatus(MTConnect mtc){

		AdapterVo pureStatusVo = new AdapterVo();
		
		Long statusUnixSec = CommonFunction.mtcDateTime2Mil(mtc.getCreationTime());
		pureStatusVo.setStartDateTime(CommonFunction.unixTime2Datetime(statusUnixSec));
		//logger.error("@pureStatusVo.getStartDateTime()@:["+pureStatusVo.getStartDateTime()+"]");

		pureStatusVo.setDvcId(mtc.getSender());
		pureStatusVo.setSender(mtc.getSender());
		
		pureStatusVo.setStatus(mtc.getStatus());
		if("IOLOGIK".equals(mtc.getStatus())){
			pureStatusVo.setIoLogik(mtc.getIoLogik());
		}else{
			pureStatusVo.setIoLogik("000000");
		}
				
		pureStatusVo.setMainPrgmName(setLimitStr(mtc.getMainPrgmName(),20));
		pureStatusVo.setCrntPrgmName(setLimitStr(mtc.getCrntPrgmName(),20));
		pureStatusVo.setPrgmHead(setLimitStr(mtc.getPrgmHead(),50));
				
		//logger.info("mtc.getAlarmMsg1():"+mtc.getAlarmMsg1());
		
		pureStatusVo.setAlarmMsg1(setLimitStr(mtc.getAlarmMsg1(),100));
		pureStatusVo.setAlarmMsg2(setLimitStr(mtc.getAlarmMsg2(),100));
		pureStatusVo.setAlarmMsg3(setLimitStr(mtc.getAlarmMsg3(),100));

		pureStatusVo.setAlarmNum1(setLimitStr(mtc.getAlarmNum1(),20));
		pureStatusVo.setAlarmNum3(setLimitStr(mtc.getAlarmNum2(),20));
		pureStatusVo.setAlarmNum2(setLimitStr(mtc.getAlarmNum3(),20));
		
		pureStatusVo.setMdlM1(setLimitStr(mtc.getMdlM1(),11));
		pureStatusVo.setMdlM2(setLimitStr(mtc.getMdlM2(),11));
		pureStatusVo.setMdlM3(setLimitStr(mtc.getMdlM3(),11));
		
		pureStatusVo.setMdlT(setLimitStr(mtc.getMdlT(),11));
		pureStatusVo.setMdlD(setLimitStr(mtc.getMdlD(),11));
		pureStatusVo.setMdlH(setLimitStr(mtc.getMdlH(),11));
		
		if(mtc.getSpdLd() == null
			|| mtc.getSpdLd().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getSpdLd().equals("NaN")
				) {
			pureStatusVo.setSpdLd(null);
		}else{
			pureStatusVo.setSpdLd(Float.parseFloat(mtc.getSpdLd()));
			if (Float.valueOf(mtc.getSpdLd()) == 0 ){
				pureStatusVo.setIsZeroSpdLoad(true);
			}else{ //( Float.valueOf(mtc.getSpindle_load()) > 0 ){
				pureStatusVo.setIsZeroSpdLoad(false);
			}
		}
		
		if(mtc.getFdOvrd() == null
			|| mtc.getFdOvrd().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getFdOvrd().equals("NaN")
			) {
			pureStatusVo.setFdOvrd(null);
		}else{
			pureStatusVo.setFdOvrd((int)Double.parseDouble(mtc.getFdOvrd()));
			if (Float.valueOf(mtc.getFdOvrd()) == 0 ){
				pureStatusVo.setIsZeroFdOvrd(true);
			}else{// if(Integer.valueOf(mtc.getFdOvrd()) > 0){
				pureStatusVo.setIsZeroFdOvrd(false);
			}
		}
		
		if(mtc.getRpdOvrd() == null
			|| mtc.getRpdOvrd().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getRpdOvrd().equals("NaN")
				) {
			pureStatusVo.setRpdFdOvrd(null);
		}else{
			pureStatusVo.setRpdFdOvrd((int)Double.parseDouble(mtc.getRpdOvrd()));
		}
		
		if(mtc.getSpdOvrd() == null
			|| mtc.getSpdOvrd().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getSpdOvrd().equals("NaN")
			) {
			pureStatusVo.setSpdOvrd(null);
		}else{
			pureStatusVo.setSpdOvrd((int)Double.parseDouble(mtc.getSpdOvrd()));
		}
		
		if(mtc.getActFd() == null
			|| mtc.getActFd().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getActFd().equals("NaN")
			) {
			pureStatusVo.setActFd(null);
		}else{
			pureStatusVo.setActFd((int)Float.parseFloat(mtc.getActFd()));
			if (Float.valueOf(mtc.getActFd()) == 0 ){
				pureStatusVo.setIsZeroActFd(true);
			}else{// if(Integer.valueOf(mtc.getActual_feed()) > 0){
				pureStatusVo.setIsZeroActFd(false);
			}
		}
		
		if(mtc.getSpdActSpeed() == null
			|| mtc.getSpdActSpeed().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getSpdActSpeed().equals("NaN")
			) {
			pureStatusVo.setSpdActSpeed(null);
		}else{
			pureStatusVo.setSpdActSpeed((int)(Float.parseFloat(mtc.getSpdActSpeed()) ));
			if (Float.valueOf(mtc.getSpdActSpeed()) == 0 ){
				pureStatusVo.setIsZeroActSpd(true);
			}else{ //( Float.valueOf(mtc.getSpindle_load()) > 0 ){
				pureStatusVo.setIsZeroActSpd(false);
			}
		}

		pureStatusVo.setMode(setLimitStr(mtc.getMode(),11));
		
		pureStatusVo.setWorkDate(CommonFunction.mil2WorkDate(statusUnixSec));
		
		//이부분 때문에 기준시간 변경됨,
		//pureStatusVo.setWorkDate(CommonFunction.utcIsoDt2UtcWorkDate_(mtc.getCreationTime()));
		
		if(mtc.getMainPrgmStartTime() == null
				|| mtc.getMainPrgmStartTime().equals(CommonCode.MSG_UNAVAIL)
				|| mtc.getSpdLd().equals("NaN")
		) {
			pureStatusVo.setMainPrgmStartTime(null);
		}else{
			if(mtc.getMainPrgmStartTime().equals("0")){
				pureStatusVo.setMainPrgmStartTime(null);
			}else{
				pureStatusVo.setMainPrgmStartTime(CommonFunction.unixTime2Datetime(Long.parseLong(mtc.getMainPrgmStartTime())));
				logger.info("MTC_MainProgramStartTime:"+mtc.getMainPrgmStartTime());
				logger.info("PURE_MainProgramStartTime:"+pureStatusVo.getMainPrgmStartTime());
			}
		}
		if(mtc.getMainPrgmStartTime2() == null
				|| mtc.getMainPrgmStartTime2().equals(CommonCode.MSG_UNAVAIL)
				|| mtc.getSpdLd().equals("NaN")
		) {
			pureStatusVo.setMainPrgmStartTime2(null);
		}else{
			if(mtc.getMainPrgmStartTime2().equals("0")){
				pureStatusVo.setMainPrgmStartTime2(null);
			}else{
				pureStatusVo.setMainPrgmStartTime2(CommonFunction.unixTime2Datetime(Long.parseLong(mtc.getMainPrgmStartTime2())));
			}
		}
		
		if(mtc.getPartCount() == null
			|| mtc.getPartCount().equals(CommonCode.MSG_UNAVAIL)
			|| mtc.getPartCount().equals("NaN")
			) {
			pureStatusVo.setPartCount(null);
		}else{
			pureStatusVo.setPartCount((Integer.parseInt(mtc.getPartCount()) ));
		}
		
		if(mtc.getTotalLineNumber() == null
				|| mtc.getTotalLineNumber().equals(CommonCode.MSG_UNAVAIL)
				|| mtc.getTotalLineNumber().equals("NaN")
			) {
			pureStatusVo.setTotalLineNumber(null);
		}else{
			pureStatusVo.setTotalLineNumber((Integer.parseInt(mtc.getTotalLineNumber()) ));
		}
		
		if(mtc.getLine() == null
				|| mtc.getLine().equals(CommonCode.MSG_UNAVAIL)
				|| mtc.getLine().equals("NaN")
				) {
				pureStatusVo.setLine(null);
		}else{
			pureStatusVo.setLine((Integer.parseInt(mtc.getLine()) ));
		}
		
		pureStatusVo.setBlock(setLimitStr(mtc.getBlock(),50));
		
		
		return pureStatusVo;
	}
	
	private AdapterVo setAgtChartStatus(AdapterVo inputVo, List <McPrgmVo> listPrgm){
		String status = inputVo.getStatus();
		
		int cntAlarm = getAlarmCnt(inputVo);
		
		int cntApc = getCntAPCPrgm(inputVo);
		inputVo.setAlarmCnt(cntAlarm);
		
		logger.info("cntAlarm:"+cntAlarm);
		logger.info("cntApc:"+cntApc);
		if(null != inputVo.getChartStatus() && inputVo.getChartStatus().equals(CommonCode.MSG_DATE_START)){
			logger.info("CHART_STATUS CHECK STEP1:"+inputVo.getChartStatus());
			return inputVo;
		}else if( cntAlarm > 0
				&& inputVo.getIsZeroSpdLoad()  // 확인 필요
				&& inputVo.getIsZeroActSpd() ) // 확인 필요
		{
			logger.info("CHART_STATUS CHECK STEP2:"+cntAlarm);
			logger.info("CHART_STATUS CHECK STEP2:"+inputVo.getIsZeroSpdLoad());
			logger.info("CHART_STATUS CHECK STEP2:"+inputVo.getIsZeroActSpd());
			inputVo.setChartStatus(CommonCode.MSG_ALARM);
		}else if(cntApc > 0){
			logger.info("CHART_STATUS CHECK STEP3:"+cntApc);
			inputVo.setChartStatus(CommonCode.MSG_WAIT);
		}else if(! (inputVo.getMode().equals("MEM") || inputVo.getMode().equals("MEMORY")|| inputVo.getMode().equals("REMOTE") )){
			logger.info("CHART_STATUS CHECK STEP4:"+inputVo.getMode());
			inputVo.setChartStatus(CommonCode.MSG_WAIT);
		}else if(( !( status.equals("STRT") || status.equals("START") || status.equals("AUTO")) ) && cntAlarm < 1){
			logger.info("CHART_STATUS CHECK STEP5:"+status);
			inputVo.setChartStatus(CommonCode.MSG_WAIT);
		}else if( "1".equals(inputVo.getMdlM1())||"1".equals(inputVo.getMdlM2())||"1".equals(inputVo.getMdlM3())
					||"01".equals(inputVo.getMdlM1())||"01".equals(inputVo.getMdlM2())||"01".equals(inputVo.getMdlM3())
					){
			logger.info("CHART_STATUS CHECK STEP6:"+inputVo.getMdlM1());
			logger.info("CHART_STATUS CHECK STEP6:"+inputVo.getMdlM2());
			logger.info("CHART_STATUS CHECK STEP6:"+inputVo.getMdlM3());
			inputVo.setChartStatus(CommonCode.MSG_WAIT);
		}else if( "0".equals(inputVo.getMdlM1())
				||"00".equals(inputVo.getMdlM1())
				){
			logger.info("CHART_STATUS CHECK STEP7:"+inputVo.getMdlM1());
			inputVo.setChartStatus(CommonCode.MSG_WAIT);
		}else if( "60".equals(inputVo.getMdlM1())||"60".equals(inputVo.getMdlM2())||"60".equals(inputVo.getMdlM3())
				){
			logger.info("CHART_STATUS CHECK STEP8:"+inputVo.getMdlM1());
			logger.info("CHART_STATUS CHECK STEP8:"+inputVo.getMdlM2());
			logger.info("CHART_STATUS CHECK STEP8:"+inputVo.getMdlM3());
			inputVo.setChartStatus(CommonCode.MSG_WAIT);
		}else if( "30".equals(inputVo.getMdlM1())||"30".equals(inputVo.getMdlM2())||"30".equals(inputVo.getMdlM3())
				){
			logger.info("CHART_STATUS CHECK STEP9:"+inputVo.getMdlM1());
			logger.info("CHART_STATUS CHECK STEP9:"+inputVo.getMdlM2());
			logger.info("CHART_STATUS CHECK STEP9:"+inputVo.getMdlM3());
			inputVo.setChartStatus(CommonCode.MSG_WAIT);
		}else if( ( status.equals("STRT") || status.equals("START") || status.equals("AUTO")) ){
			logger.info("CHART_STATUS CHECK STEP10:"+inputVo.getMdlM3());
			inputVo.setChartStatus(CommonCode.MSG_IN_CYCLE);	
		}else {//(status.equals("STRT") || status.equals("START")){
			inputVo.setChartStatus(CommonCode.MSG_WAIT);
		}
	
		logger.info("END CONDITION:"+inputVo.getChartStatus());
		return inputVo;
	}
	
	private int getMainPrgmCnt(AdapterVo inputVo, List <McPrgmVo> listPrgm){
		int rtnCnt=0;
		for(McPrgmVo i : listPrgm){
			if(CommonCode.MSG_PRGM_TYPE_MAIN.equals(i.getPrgmType()) && inputVo.getMainPrgmName().equals(i.getName()) ){
				rtnCnt++;
			}
		}
		return rtnCnt;
	}
	private int getPtlChgPrgmCnt(AdapterVo inputVo, List <McPrgmVo> listPrgm){
		int rtnCnt=0;
		for(McPrgmVo i : listPrgm){
			if(CommonCode.MSG_PRGM_TYPE_PALLET_CHAGE.equals(i.getPrgmType()) && inputVo.getCrntPrgmName().equals(i.getName()) ){
				rtnCnt++;
			}
		}
		return rtnCnt;
	}
	
	private AdapterVo isDuple(AdapterVo preVo, AdapterVo inputVo){
		Long preStartTime = 0L;
		if(null == preVo.getStartDateTime()){
			preStartTime = CommonFunction.dateTime2Mil(preVo.getStartDateTime());
		}else{
			preStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		}
		Long crtStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		
		if(crtStartTime<preStartTime){
			logger.error("timeTrash data");
			return null;
		}
		//logger.info("before duple check");
		//logger.info("preVo:"+preVo);
		//logger.info("inputVo:"+inputVo);

		if(preVo.getAlarmNum1().equals(inputVo.getAlarmNum1())
			&&preVo.getAlarmNum2().equals(inputVo.getAlarmNum2())
			&&preVo.getAlarmNum3().equals(inputVo.getAlarmNum3())
			&&preVo.getFdOvrd().equals(inputVo.getFdOvrd())
			&&preVo.getMode().equals(inputVo.getMode())
			&&preVo.getIsZeroSpdLoad().equals(inputVo.getIsZeroSpdLoad())
			&&preVo.getIsZeroFdOvrd().equals(inputVo.getIsZeroFdOvrd())
			&&preVo.getIsZeroActFd().equals(inputVo.getIsZeroActFd())
			&&preVo.getPrgmHead().equals(inputVo.getPrgmHead())
			&&preVo.getMainPrgmName().equals(inputVo.getMainPrgmName())			
			&&preVo.getMdlM1().equals(inputVo.getMdlM1())
			&&preVo.getMdlM2().equals(inputVo.getMdlM2())
			&&preVo.getMdlM3().equals(inputVo.getMdlM3())
		)
		{
			logger.info("Duple");
	    	return null;
		}else{
    		return inputVo;
		}
	}
	
	private String addDeviceChartStatusData(DeviceStatusVo inputVo){
		deviceService.adjustDeviceChartStatus_SP(inputVo);
		return "OK";
	}
	
	private AdapterVo setIsZeroStatus(AdapterVo inputVo){
		logger.info("run setIsZeroStatus");
		if (Float.valueOf(inputVo.getSpdLd()) == 0 ){
			inputVo.setIsZeroSpdLoad(true);
		}else{
			inputVo.setIsZeroSpdLoad(false);
		}
		
		if (Integer.valueOf(inputVo.getFdOvrd()) == 0 ){
			inputVo.setIsZeroFdOvrd(true);
		}else{
			inputVo.setIsZeroFdOvrd(false);
		}
		
		logger.info("getActFd:"+inputVo.getActFd());
		if(inputVo.getActFd()==null){
			logger.info("NULL!");
		}
			
		if (Float.valueOf(inputVo.getActFd()) == 0 ){
			inputVo.setIsZeroActFd(true);
		}else{
			inputVo.setIsZeroActFd(false);
		}
		
		return inputVo;
	}
	
	private int getAlarmCnt(AdapterVo inputVo){
		DeviceVo rtnVo = new DeviceVo();
		
		List<String> listAlarmCode = new ArrayList<String>();
		List<String> listAlarmMsg = new ArrayList<String>();
		
		int rtnCnt=0;
		
		if(! (inputVo.getAlarmNum1() == null || inputVo.getAlarmNum1().equals(CommonCode.MSG_UNAVAIL))){
			listAlarmCode.add(inputVo.getAlarmNum1());
			listAlarmMsg.add(inputVo.getAlarmMsg1());
		}
		
		if(! (inputVo.getAlarmNum2() == null || inputVo.getAlarmNum2().equals(CommonCode.MSG_UNAVAIL))){
			listAlarmCode.add(inputVo.getAlarmNum2());
			listAlarmMsg.add(inputVo.getAlarmMsg2());
		}
		if(! (inputVo.getAlarmNum3() == null || inputVo.getAlarmNum3().equals(CommonCode.MSG_UNAVAIL))){
			listAlarmCode.add(inputVo.getAlarmNum3());
			listAlarmMsg.add(inputVo.getAlarmMsg3());
		}
		
		//fix 160307
		if(listAlarmCode.size() > 0){
			rtnVo.setLastAlarmCode(listAlarmCode.get(listAlarmCode.size()-1));
			rtnVo.setLastAlarmMsg(listAlarmMsg.get(listAlarmMsg.size()-1));
			rtnCnt = listAlarmCode.size();
		}else{
			rtnVo.setLastAlarmCode(CommonCode.MSG_UNAVAIL);
			rtnVo.setLastAlarmMsg(CommonCode.MSG_UNAVAIL);
		}
		
		return rtnCnt;
	}
	
	private MTConnect getMTCfromXML(String inputXML){
		MTConnect mtConnect = new MTConnect();
		//Only ASCII
		//inputXML = inputXML.replaceAll("[^\\x20-\\x7e]", "");
//		logger.info("inputXML:" + inputXML);
		DocumentBuilder db=null;
		try {

			try {
				db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Document doc = db.parse(new InputSource(new ByteArrayInputStream(inputXML.getBytes("utf-8"))));
			RogerParser parser = new RogerParser(doc);
			
			parser.Init();
			parser.execute(mtConnect);
			
			logger.info("CRTime:"+mtConnect.getCreationTime());
			logger.info("Sender:"+mtConnect.getSender());
			logger.info("Status:"+mtConnect.getStatus());
			logger.info("FdOvrd:"+mtConnect.getFdOvrd());
			logger.info("RpdFdOvrd:"+mtConnect.getRpdOvrd());
			logger.info("SpdOvrd:"+mtConnect.getSpdOvrd());

			logger.info("MainPrgmName:"+mtConnect.getMainPrgmName());
			logger.info("MainPrgmStrtTime:"+mtConnect.getMainPrgmStartTime());
			logger.info("MainPrgmStrtTime2:"+mtConnect.getMainPrgmStartTime2());
			logger.info("Mode:"+mtConnect.getMode());
			
			logger.info("Mode:"+mtConnect.getMode());
			logger.info("MdlM1:"+mtConnect.getMdlM1());
			logger.info("MdlM2:"+mtConnect.getMdlM2());
			logger.info("MdlM3:"+mtConnect.getMdlM3());
			logger.info("MdlT:"+mtConnect.getMdlT());
			logger.info("MdlD:"+mtConnect.getMdlD());
			logger.info("MdlH:"+mtConnect.getMdlH());
			
			logger.info("AlarmMsg1"+mtConnect.getAlarmMsg1());
			logger.info("AlarmMsg2"+mtConnect.getAlarmMsg2());
			logger.info("AlarmMsg3"+mtConnect.getAlarmMsg3());
			
			logger.info("AlarmNum1"+mtConnect.getAlarmNum1());
			logger.info("AlarmNum2"+mtConnect.getAlarmNum2());
			logger.info("AlarmNum3"+mtConnect.getAlarmNum3());
			
			logger.info("partCount"+mtConnect.getPartCount());
			logger.info("totalLineNumber"+mtConnect.getTotalLineNumber());
			logger.info("Line"+mtConnect.getLine());
			logger.info("Block"+mtConnect.getBlock());
			
			
			//logger.info("AlarmNum3"+mtConnect.getAlarmNum3());
			
			logger.info("PrgmHead:"+mtConnect.getPrgmHead());
			
			
		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.error(e.toString());
		}
		return mtConnect;
	}
	
	@RequestMapping(value = "mqt")
	@ResponseBody
	public String mqt(HttpServletRequest request){
		
		String rtnStr = deviceService.mqt();
		
		return rtnStr;
	}
	
	String setLimitStr(String input,int limit){
		String rtnStr;
		if(input == null
				|| input.equals(CommonCode.MSG_UNAVAIL)
				|| input.equals("NaN")
			) {
			rtnStr = null;
		}else{
			int tmpLength = input.length();
			rtnStr = input.substring(0, (tmpLength > limit)?limit:tmpLength);
		}
		
		return rtnStr;
	}
	
	private int getCntAPCPrgm(AdapterVo inputVo){
		int rtnInt = 0;
		
		String prgmApc = CommonCode.MAP_MSG_APC_PRGM.get(inputVo.getDvcId());
		if(inputVo.getMainPrgmName().equals(prgmApc)){
			rtnInt++;
		}
		
		return rtnInt;
	}
	
}
