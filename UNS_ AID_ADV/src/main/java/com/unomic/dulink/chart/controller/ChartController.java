package com.unomic.dulink.chart.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.service.ChartService;

@RequestMapping(value = "/chart")
@Controller
public class ChartController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChartController.class);
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	@Autowired
	private ChartService chartService;

	@RequestMapping(value="testChart")
	public String testChart(){
		return "chart/testChart";
	};
	
	@RequestMapping(value="demo")
	public String demo(){
		return "chart/demo";
	};
	
	@RequestMapping(value="getAllDvcList")
	@ResponseBody
	public String getAllDvcList(){
		String str = "";
		try {
			str = chartService.getAllDvcList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getLastConnection")
	@ResponseBody
	public ChartVo getLastConnection(ChartVo chartVo){
		try {
			chartVo = chartService.getLastConnection(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	};
	
	@RequestMapping(value="getTotalOpRatio")
	@ResponseBody
	public String getTotalOpRatio(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getTotalOpRatio(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="timeLine")
	public String timeLine (){
		return "chart/timeLine";
	};
	
	@RequestMapping(value="timeLine2")
	public String timeLine2 (){
		return "chart/timeLine2";
	};
	
	@RequestMapping(value="getAllEvt")
	@ResponseBody
	public String getAllEvt(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getAllEvt(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getStartTime")
	@ResponseBody
	public String getStartTime(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getStartTime(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getAlarmList")
	@ResponseBody
	public String getAlarmList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getAlarmList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getMonthlyRailTatget")
	@ResponseBody
	public String getMonthlyRailTatget(){
		String str = "";
		try {
			str = chartService.getMonthlyRailTatget();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getMonthlyBlockTatget")
	@ResponseBody
	public String getMonthlyBlockTatget(){
		String str = "";
		try {
			str = chartService.getMonthlyBlockTatget();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="addMonthlyRailTarget")
	@ResponseBody
	public String addMonthlyRailTarget(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.addMonthlyRailTarget(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="addMonthlyBlockTarget")
	@ResponseBody
	public String addMonthlyBlockTarget(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.addMonthlyBlockTarget(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getStatistic")
	@ResponseBody
	public String getStatistic(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getStatistic(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getTargetData")
	@ResponseBody
	public String getTargetData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getTargetData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getTargetBlockData")
	@ResponseBody
	public String getTargetBlockData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getTargetBlockData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	
	@RequestMapping(value="addBlockTargetCnt")
	@ResponseBody
	public String addBlockTargetCnt(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.addBlockTargetCnt(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="addTargetCnt")
	@ResponseBody
	public String addTargetCnt(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.addTargetCnt(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getReportData")
	@ResponseBody
	public String getReportData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getReportData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getLeftRailChart")
	@ResponseBody
	public String getLeftRailChart(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getLeftRailChart(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value="getLeftBlockChart")
	@ResponseBody
	public String getLeftBlockChart(){
		String str = "";
		try {
			str = chartService.getLeftBlockChart();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="	getDetailRailData")
	@ResponseBody
	public String 	getDetailRailData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.	getDetailRailData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="	getDetailBlockData")
	@ResponseBody
	public String 	getDetailBlockData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.	getDetailBlockData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getMcNo")
	@ResponseBody
	public ChartVo getMcNo(ChartVo chartVo){
		try {
			chartVo = chartService.getMcNo(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	}

	@RequestMapping(value="getReportBarData")
	@ResponseBody
	public String getReportBarData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getReportBarData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="setInitCardPos")
	@ResponseBody
	public void setInitCardPos(ChartVo chartVo){
		try {
			chartService.setInitCardPos(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};
	
	@RequestMapping(value="getDvcTemper")
	@ResponseBody
	public ChartVo getDvcTemper(ChartVo chartVo){
		try {
			chartVo = chartService.getDvcTemper(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	};
	
	@RequestMapping(value="cam1")
	public String cam1(){
		return "chart/cam1";
	}

	@RequestMapping(value="cam2")
	public String cam2(){
		return "chart/cam2";
	}

	@RequestMapping(value="detailChart_rail")
	public String detailChart_rail(){
		return "chart/detailChart_rail";
	}

	@RequestMapping(value="detailChart_rail_demo")
	public String detailChart_rail_demo(){
		return "chart/detailChart_rail_demo";
	}
	
	@RequestMapping(value="getMachineData")
	@ResponseBody
	public ChartVo getMachineData(ChartVo chartVo){
		try {
			chartVo = chartService.getMachineData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	};
	
	@RequestMapping(value="getLeftTableData")
	@ResponseBody
	public String getLeftTableData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getLeftTableData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="detailChart_block")
	public String detailChart(){
		return "chart/detailChart_block";
	}
	
	@RequestMapping(value="report")
	public String report(){
		return "chart/report";
	}
	
	@RequestMapping(value="barChart")
	public String barChart(){
		return "chart/barChart";
	}
	
	@RequestMapping(value="cam3")
	public String cam3(){
		return "chart/cam3";
	}
	@RequestMapping(value="getMachineStatus")
	@ResponseBody
	public String getMachineStatus(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getMachineStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="setCardPos")
	@ResponseBody
	public void setCardPos(ChartVo chartVo){
		try {
			chartService.setCardPos(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
	};
	
	@RequestMapping(value="getAllMachine")
	@ResponseBody
	public String getAllMachine(){
		String str = "";
		try {
			str = chartService.getAllMachine();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		
		return str;
	};
	
	@RequestMapping(value="getAllBlockMachine")
	@ResponseBody
	public String getAllBlockMachine(){
		String str = "";
		try {
			str = chartService.getAllBlockMachine();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		
		return str;
	};
	
	@RequestMapping(value="setEmoCardPos")
	@ResponseBody
	public void setEmpCardPos(ChartVo chartVo){
		try {
			chartService.setEmoCardPos(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
	};
	
	@RequestMapping(value="cardPage")
	public String cardPage(ChartVo chartVo, HttpServletRequest request){
		request.setAttribute("folderId", chartVo.getFolderId());
		return "chart/cardPage";
	};
	
	@RequestMapping(value="getFolderList")
	@ResponseBody
	public String getFolderList(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getFolderList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="makeFolder")
	@ResponseBody
	public String makeFolder(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.makeFolder(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="modifyFolderName")
	@ResponseBody
	public void modifyFolderName(ChartVo chartVo){
		try {
			chartService.modifyFolderName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
	};
	
	@RequestMapping(value="modifyMachineName")
	@ResponseBody
	public void modifyMachineName(ChartVo chartVo){
		try {
			chartService.modifyMachineName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};
	
	@RequestMapping(value="getCardsList")
	@ResponseBody
	public String getCardsList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getCardsList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="removeCard")
	@ResponseBody
	public void removeCard(ChartVo chartVo){
		try {
			chartService.removeCard(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};
	
	@RequestMapping(value="getMachineList")
	@ResponseBody
	public String getMachineList(){
		String rtn = "";
		try {
			rtn = chartService.getMachineList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rtn;
	};
	
	@RequestMapping(value="getEmoMachineList")
	@ResponseBody
	public String getEmoMachineList(ChartVo chartVo){
		String rtn = "";
		try {
			rtn = chartService.getEmoMachineList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rtn;
	};
	
	
	@RequestMapping(value="report2")
	public String report2(){
		return "chart/report2";
	};
	
	@RequestMapping(value="report3")
	public String report3(){
		return "chart/report3";
	};
	
	@RequestMapping(value="report4")
	public String report4(){
		return "chart/report4";
	};
	
	@RequestMapping(value="report5")
	public String report5(){
		return "chart/report5";
	};
	
	@RequestMapping(value="report6")
	public String report6(){
		return "chart/report6";
	};
	
	@RequestMapping(value="report7")
	public String report7(){
		return "chart/report7";
	};
	
	@RequestMapping(value="IE")
	public String IE(){
		return "chart/dashBoard_IE";
	};
	
	@RequestMapping(value="highChartTest")
	public String highChartTest(){
		return "chart/highChartTest";
	};
	
	@RequestMapping(value="dimf")
	public String dkimf(){
		return "chart/DIMF";
	};
	
	@RequestMapping(value="edit")
	public String edit(){
		return "chart/dashBoard_edit";
	};
	
	@RequestMapping(value="main")
	public String main(){
		return "chart/factory911_index";
	};

	@RequestMapping(value="chart1")
	public String chart1(){
		return "chart/chart1";
	};
	
	@RequestMapping(value="chart_layout")
	public String chart_layout(){
		return "chart/chart_layout";
	};
	
	@RequestMapping(value="chart2")
	public String chart2(){
		return "chart/chart2";
	};
	
	@RequestMapping(value="chart_rail")
	public String chart_real(){
		return "chart/chart_rail";
	};
	
	@RequestMapping(value="chart_rail_demo")
	public String chart_real_demo(){
		return "chart/chart_rail_demo";
	};
	
	@RequestMapping(value="chart_block")
	public String chart_block(){
		return "chart/chart_block";
	};
	
	@RequestMapping(value="main1")
	public String main1(){
		return "chart/dashBoard";
	};
	
	@RequestMapping(value="dashBoard")
	public String dashBoard(){
		return "chart/dashBoard_sample";
	};
	
	@RequestMapping(value="multiVision")
	public String multiVision(){
		return "chart/multiVision";
	};
	
	@RequestMapping(value="index")
	public String index(){
		return "chart/index";
	};
	
	@RequestMapping(value="main2")
	public String main2(){
		return "chart/dashBoard2";
	};
	
	@RequestMapping(value="main3")
	public String main3(){
		return "chart/dashBoard3";
	};
	
	@RequestMapping(value="main4")
	public String main4(){
		return "chart/dashBoard4";
	};
	
	@RequestMapping(value="mobile")
	public String mobile(HttpServletRequest request){
//		String dvcId = request.getParameter("dvcId");
//		request.setAttribute("dvcId", dvcId);
		return "chart/mobile";
	};

	@RequestMapping(value="mobile2")
	public String mobile2(HttpServletRequest request){
		String dvcId = request.getParameter("dvcId");
		request.setAttribute("dvcId", dvcId);
		return "chart/mobile2";
	};
	
	@RequestMapping(value="getDvcData")
	@ResponseBody
	public ChartVo getDvcData(ChartVo chartVo){
		try {
			chartVo = chartService.getDvcData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	};
	
//	@RequestMapping(value="mobile2")
//	public String mobile2(){
//		return "chart/mobile2";
//	};
	
	@RequestMapping(value="highChart1")
	public String highChart1(){
		return "chart/highChart1";
	};
	
	@RequestMapping(value="highChart2")
	public String highChart2(){
		return "chart/highChart2";
	}
	
	@RequestMapping(value="highChart3")
	public String highChart3(){
		return "chart/highChart3";
	};
	
	@RequestMapping(value="highChart4")
	public String highChart4(){
		return "chart/highChart4";
	};
	
	@RequestMapping(value="video")
	public String video(){
		return "test/video";
	}
	
	@RequestMapping(value="video2")
	public String video2(){
		return "test/video2";
	};
	
	@RequestMapping(value="getStatusData")
	@ResponseBody
	public String getStatusData(ChartVo chartVo){
		String result = "";
		try {
			Calendar cal = Calendar.getInstance ( );//오늘 날짜를 기준으루..
			//cal.add ( cal.DATE, 1); //2개월 전....
			String year = String.valueOf(cal.get ( cal.YEAR ));
			String month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
			String date = addZero(String.valueOf(cal.get ( cal.DATE )));
			
			String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
			String minute = String.valueOf(cal.get(cal.MINUTE));
			String second = String.valueOf(cal.get(cal.SECOND));
			chartVo.setDate(year + "-" + month + "-" + date);
			
			if(Integer.parseInt(hour)>=20){
				cal.add ( cal.DATE, 1);
				year = String.valueOf(cal.get ( cal.YEAR ));
				month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
				date = addZero(String.valueOf(cal.get ( cal.DATE )));
				
				chartVo.setDate(year + "-" + month + "-" + date);
			};
			//chartVo.setDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));
			chartVo.setDate("2015-05-28");
			result = chartService.getStatusData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//result = chartVo.getCallback() + "(" + result + ")";
		return result;
	};
	
	@RequestMapping(value="test")
	@ResponseBody
	public void test(){
		ChartVo chartVo = new ChartVo();
		chartVo.setDate("2015-05-09");
		try {
			chartService.test(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@RequestMapping(value="getCurrentDvcData")
	@ResponseBody
	public ChartVo getCurrentDvcData(ChartVo chartVo){
		try {
			Calendar cal = Calendar.getInstance ( );//오늘 날짜를 기준으루..
			//cal.add ( cal.DATE, 1); //2개월 전....
			
			String year = String.valueOf(cal.get ( cal.YEAR ));
			String month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
			String date = addZero(String.valueOf(cal.get ( cal.DATE )));
			
			String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
			String minute = String.valueOf(cal.get(cal.MINUTE));
			String second = String.valueOf(cal.get(cal.SECOND));
			chartVo.setDate(year + "-" + month + "-" + date);
			
			if(Integer.parseInt(hour)>=20){
				cal.add ( cal.DATE, 1);
				year = String.valueOf(cal.get ( cal.YEAR ));
				month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
				date = addZero(String.valueOf(cal.get ( cal.DATE )));
				
				chartVo.setDate(year + "-" + month + "-" + date);
			};
			
			chartVo = chartService.getCurrentDvcData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return chartVo;
	};
	
	//@Scheduled(fixedDelay = 5000)
	public void addData(){
		try {
			chartService.addData();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};
	
	@RequestMapping(value="getMachineName")
	@ResponseBody
	public String getMachineName(ChartVo chartVo){
		String rtn = "";
		try {
			rtn = chartService.getMachineName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rtn;
	};
	
	@RequestMapping(value="getTimeData")
	@ResponseBody
	public String getTimeData(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getTimeData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getDvcName")
	@ResponseBody
	public String getDvcName(ChartVo chartVo){
		String dvcName = null;
		try {
			dvcName = chartService.getDvcName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dvcName;
	};
	
	@RequestMapping(value="getAllDvcStatus")
	@ResponseBody
	public String getAllDvcStatus(ChartVo chartVo){
		String result = null;
		try {
			result = chartService.getAllDvcStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getAllDvcId")
	@ResponseBody
	public String getAllDvcId(ChartVo chartVo){
		String result = null;
		try {
			result = chartService.getAllDvcId();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	
	@RequestMapping(value="getBarChartDvcId")
	@ResponseBody
	public String getBarChartDvcId2(ChartVo chartVo){
		String result = null;
		try {
			result = chartService.getBarChartDvcId();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	public String addZero(String str){
		String rtn = str;
		if(rtn.length()==1){
			rtn = "0" + str;
		};
		return rtn;
	};
	
	@RequestMapping(value="getTime")
	@ResponseBody
	public String getTime(){
		String rtn = "";

		Calendar cal = Calendar.getInstance ( );//오늘 날짜를 기준으루..
		//cal.add ( cal.DATE, 1); //2개월 전....
		
		String year = String.valueOf(cal.get ( cal.YEAR ));
		String month = String.valueOf(cal.get ( cal.MONTH )+1);
		String day = String.valueOf(cal.get ( cal.DATE ));
		
		String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
		String minute = String.valueOf(cal.get(cal.MINUTE));
		String second = String.valueOf(cal.get(cal.SECOND));
		rtn = year + ". " + month + ". " + day + ".-" + hour + ":" + minute + ":" + second;
		return rtn;
	};
	
	@RequestMapping(value="dashBoard_pad")
	public String pad(){
		return "chart/dashBoard_pad";
	};
	
	@RequestMapping(value="dashBoard_left")
	public String pad_left(){
		return "chart/dashBoard_left";
	};
	
	@RequestMapping(value="dashBoard_right")
	public String pad_right(){
		return "chart/dashBoard_right";
	};
	
	@RequestMapping(value="dashBoard_center")
	public String center(){
		return "chart/dashBoard_center";
	};
	
	@RequestMapping(value="polling1")
	@ResponseBody
	public String polling1() throws Exception{
		return chartService.polling1();
	};
	
	@RequestMapping(value="setVideo")
	@ResponseBody
	public String setVideo(String id) throws Exception{
		return chartService.setVideo(id);
	};
	
	@RequestMapping(value="initVideoPolling")
	@ResponseBody
	public void initVideoPolling() throws Exception{
		chartService.initVideoPolling();
	}
	
	@RequestMapping(value="initPiePolling")
	@ResponseBody
	public void initPiePolling() throws Exception{
		chartService.initPiePolling();
	};
	
	@RequestMapping(value="piePolling1")
	@ResponseBody
	public String piePolling1() throws Exception{
		return chartService.piePolling1();
	};
	
	@RequestMapping(value="piePolling2")
	@ResponseBody
	public String piePolling2() throws Exception{
		return chartService.piePolling2();
	};
	
	@RequestMapping(value="setChart1")
	@ResponseBody
	public String setChart1(String id) throws Exception{
		return chartService.setChart1(id);
	};
	
	@RequestMapping(value="setChart2")
	@ResponseBody
	public String setChart2(String id) throws Exception{
		return chartService.setChart2(id);
	};
	
	@RequestMapping(value="delVideoMachine")
	@ResponseBody
	public void delVideoMachine(String id) throws Exception{
		chartService.delVideoMachine(id);
	};
	
	@RequestMapping(value="delChartMachine")
	@ResponseBody
	public void delChartMachine(String id) throws Exception{
		chartService.delChartMachine(id);
	};
	
	@RequestMapping(value="getAdapterId")
	@ResponseBody
	public String getAdapterId(ChartVo chartVo){
		String result = null;
		try {
			Calendar cal = Calendar.getInstance ( );//오늘 날짜를 기준으루..
			//cal.add ( cal.DATE, 1); //2개월 전....
			
			String year = String.valueOf(cal.get ( cal.YEAR ));
			String month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
			String date = addZero(String.valueOf(cal.get ( cal.DATE )));
			
			String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
			String minute = String.valueOf(cal.get(cal.MINUTE));
			String second = String.valueOf(cal.get(cal.SECOND));
			chartVo.setDate(year + "-" + month + "-" + date);
			
			if(Integer.parseInt(hour)>=20){
				cal.add ( cal.DATE, 1);
				year = String.valueOf(cal.get ( cal.YEAR ));
				month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
				date = addZero(String.valueOf(cal.get ( cal.DATE )));
				
				chartVo.setDate(year + "-" + month + "-" + date);
			};
			
			//result = chartService.getAllDvcId(chartVo);
			result = chartService.getAdapterId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="showDetailEvent")
	@ResponseBody
	public String showDetailEvent(ChartVo chartVo) throws Exception{
		return chartService.showDetailEvent(chartVo);
	};
	
//	@RequestMapping(value="getMachinePosIE")
//	@ResponseBody
//	public ChartVo getMachinePosIE(ChartVo chartVo) throws Exception{
//		return chartService.getMachinePosIE(chartVo);
//	}
	
	@RequestMapping(value="getMachinePos")
	@ResponseBody
	public ChartVo getMachinePos(ChartVo chartVo) throws Exception{
		return chartService.getMachinePos(chartVo);
	};
	
	@RequestMapping(value="setMachinePosIE")
	@ResponseBody
	public void setMachinePosIE(ChartVo chartVo) throws Exception{
		chartService.setMachinePosIE(chartVo);
	}
	
	@RequestMapping(value="addFolderid")
	@ResponseBody
	public String addFolderid(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.addFolderid(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
};

