package com.unomic.dulink.chart.service;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.adapter.domain.LampVO;
import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.common.domain.CommonFunction;


@Service
@Repository
public class ChartServiceImpl implements ChartService{
	private final static String namespace= "com.unomic.dulink.chart.";
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;

	Map statusMap = new HashMap();
	int chartOrder = 0;
	public boolean chkOrderStatus(String status){
		boolean result = false;
//		if(chartOrder<=Integer.parseInt((String) statusMap.get(status))){
//			chartOrder = Integer.parseInt((String) statusMap.get(status));
//			result = true;
//		};
	return result;
	};
	
	
	public String addZero(String str){
		if(str.length()==1){
			return "0" + str;
		}else{
			return str;
		}
	};
	
	@Override
	public String getStatusData(ChartVo chartVo) throws Exception {
		
		List<ChartVo> chartData = sql_ma.selectList(namespace + "getStatusData", chartVo);
		 // List<ChartVo> chartData = sql.selectList(namespace + "getTestData");
		//System.out.println("getStatusData"+ ( endTime - startTime )/1000.0f +"초");
		
		List list = new ArrayList();
		
		int prevTime = -1;
		String prevStatus = "";
		boolean startFlag = true;
		double block = 1/6f;
		double bar = 0;
		int spdTime = 0;
		int waitTime = 0;
		int alarmTime = 0;
		int offTime = 0;
		int inCycleTime = 0;
		
		statusMap.put("NO-CONNECTION", 0);
		statusMap.put("IN-CYCLE", 1);
		statusMap.put("WAIT", 2);
		statusMap.put("ALARM", 3);
		
		for(int i = 0; i < chartData.size(); i++){
			int startH = Integer.parseInt(chartData.get(i).getStartDateTime().substring(11,13));
			//int startM = Integer.parseInt(chartData.get(i).getStartDateTime().substring(14,16));
			String startM = addZero(chartData.get(i).getStartDateTime().substring(14,16));
			int endH = Integer.parseInt(chartData.get(i).getEndDateTime().substring(11,13));
			//int endM = Integer.parseInt(chartData.get(i).getEndDateTime().substring(14,16));
			String endM = addZero(chartData.get(i).getEndDateTime().substring(14,16));
			
			String status = chartData.get(i).getChartStatus();
			
			int start = startH * 60 + Integer.parseInt(String.valueOf(startM).substring(0,1) + "0");
			int end = endH * 60 + Integer.parseInt(String.valueOf(endM).substring(0,1) + "0");
			
			bar = Math.floor((end-start)/60) + (end-start)%60/10*block;
			
			if(start>end){
				//System.out.println("over 20");
				end = ((endH+24)*60) + Integer.parseInt(String.valueOf(endM).substring(0,1)+"0");
				bar = Math.floor((end-start)/60) + (end-start)%60/10*block;
			};
			//System.out.println("bar size : " + bar);
			//경과 시간이 1분 미만
			if((double)bar==0.0){
				bar = block;
			};
			
			if(chartData.get(i).getSpdLoad()!=null){
				if(  (!chartData.get(i).getSpdLoad().equals("0") || !chartData.get(i).getSpdLoad().equals("0.0"))
					&& chartData.get(i).getChartStatus().toUpperCase().equals("IN-CYCLE")){
					spdTime += CommonFunction.getSecDiff(chartData.get(i).getStartDateTime(), chartData.get(i).getEndDateTime());
				};
			};
			
			if(chartData.get(i).getChartStatus().toUpperCase().equals("IN-CYCLE")){
				inCycleTime += CommonFunction.getSecDiff(chartData.get(i).getStartDateTime(), chartData.get(i).getEndDateTime());
			}else if(chartData.get(i).getChartStatus().equals("WAIT")){
				waitTime += CommonFunction.getSecDiff(chartData.get(i).getStartDateTime(), chartData.get(i).getEndDateTime());
			}else if(chartData.get(i).getChartStatus().equals("ALARM")){
				alarmTime += CommonFunction.getSecDiff(chartData.get(i).getStartDateTime(), chartData.get(i).getEndDateTime());
			}else if(chartData.get(i).getChartStatus().equals("NO-CONNECTION")){
				offTime += CommonFunction.getSecDiff(chartData.get(i).getStartDateTime(), chartData.get(i).getEndDateTime());
			};
			
		//	System.out.println("prevStatus  : " + prevStatus + ", dupl ---> " + status + "->" +  startH + ":" + startM + "~" + endH + ":" + endM + ", barSize : " + bar);
			if(endH * 60 + Integer.parseInt(String.valueOf(endM).substring(0,1) + "0")-prevTime>=10 || prevTime==-1){
				chartOrder = 0;
				prevStatus = chartData.get(i).getChartStatus();
				for(int j = i; j < chartData.size(); j++){
				//	System.out.println(statusMap.get(chartData.get(i).getChartStatus()));
					//System.out.println("prevStatus  : " + prevStatus + ", dupl ---> " + status + "->" +  startH + ":" + startM + "~" + endH + ":" + endM);
				};
				
				System.out.println("prevStatus  : " + prevStatus + ", dupl ---> " + status + "->" +  startH + ":" + startM + "~" + endH + ":" + endM + ", barSize : " + bar);
				Map map = new HashMap();
				map.put("adt_id", chartData.get(i).getAdtId());
				map.put("startDateTime", startH + ":" + startM);
				map.put("endDateTime", endH + ":" + endM);
				map.put("startDateTime2", chartData.get(i).getStartDateTime());
				map.put("endDateTime2", chartData.get(i).getEndDateTime());
				map.put("chartStatus", chartData.get(i).getChartStatus());
				map.put("alarm", chartData.get(i).getAlarm());
				map.put("spdLoad", chartData.get(i).getSpdLoad());
				map.put("feedOverride", chartData.get(i).getFeedOverride());
				map.put("name", chartData.get(i).getName());
				map.put("dvcId", chartData.get(i).getDvcId());
				map.put("lastModal", chartData.get(i).getLastModal());
				map.put("operationTime", chartData.get(i).getOperationTime());
				map.put("bar",bar);
				map.put("spdTime",spdTime);
				map.put("inCycleTime", inCycleTime);
				map.put("waitTime",waitTime);
				map.put("alarmTime",alarmTime);
				map.put("offTime",offTime);
				map.put("type",chartData.get(i).getType());
				
				
				list.add(map);
			};
			
			prevTime = endH * 60 + Integer.parseInt(String.valueOf(endM).substring(0,1) + "0");
		};
		
		String str = "";
		if(list.isEmpty()){
			list.add("null");
		};
		Map map = new HashMap();
		map.put("spdTime", spdTime);
		
		Map resultMap = new HashMap();
		resultMap.put("status", list);
		resultMap.put("spdTime", map);
		
		
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		 
		return str;
	}

	@Override
	public ChartVo getCurrentDvcData(ChartVo chartVo) throws Exception {
		chartVo = (ChartVo) sql_ma.selectOne(namespace + "getCurrentDvcData", chartVo);
		//chartVo.setEndDateTime((String)sql.selectOne(namespace + "getLastUpdateTime", chartVo));
		 
		if(chartVo==null){
			chartVo = new ChartVo();
			chartVo.setChartStatus("null");
		};

		return chartVo;
	};

	@Override
	public void addData() throws Exception {
		
		ChartVo chartVo = new ChartVo();
		
		for(int i = 0; i <=1 ; i++){
			String status = "";
			int random = (int) (Math.random()*10+1) + (int) (Math.random()*10+1) + (int) (Math.random()*10+1) + (int) (Math.random()*10+1);
			if(random==40){
				status = "Alarm";
			}else if(random>=38){
				status = "Wait";
			}else{
				status = "In-Cycle";
			};
			
			Date d = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String date = sdf.format(d);
			
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time = sdf2.format(d);
			
			String spdLoad = String.valueOf(Math.floor((Math.random()*10+1) + (Math.random()*10+1)+ (Math.random()*10+1) + 60)).substring(0,2);
			if(spdLoad.equals("90")) spdLoad = "0";
			String feedOverride = String.valueOf(Math.floor((Math.random()*10+1) + (Math.random()*10+1) + (Math.random()*10+1) + 60)).substring(0,2); 
			if(feedOverride.equals("90")) spdLoad = "0";
			
			int dvcId = 0, adtId = 0;
			if(i==0){
				dvcId = 1;
				adtId = 13;
			}else if(i==1){
				dvcId = 5;
				adtId = 14;
			}
			
			chartVo.setDvcId(Integer.toString(dvcId));
			chartVo.setLastUpdateTime(time);
			chartVo.setStartDateTime(time);
			chartVo.setEndDateTime(time);
			chartVo.setAdtId(Integer.toString(adtId));
			chartVo.setSpdLoad(spdLoad);
			chartVo.setChartStatus(status);
			chartVo.setFeedOverride(feedOverride);
			chartVo.setDate(date);
			
			String chartStatus = (String) sql_ma.selectOne(namespace + "getLastChartStatus", chartVo);
			
			if(chartStatus.equals(status)){
				sql_ma.update(namespace + "updateDeviceStatus", chartVo);
			}else{
				sql_ma.update(namespace + "updateDeviceStatus", chartVo);
				chartVo.setStartDateTime((String) sql_ma.selectOne(namespace + "getStartDateTime", chartVo));
				sql_ma.update(namespace + "setChartEndTime", chartVo);
				chartVo.setStartDateTime(time);
				sql_ma.insert(namespace + "addChartStatus", chartVo);
			}
		};
	}

	@Override
	public String getDvcName(ChartVo chartVo) throws Exception {
		
		String dvcName = (String) sql_ma.selectOne(namespace + "getDvcName", chartVo);
		return dvcName;
	}

	@Override
	public String getAllDvcStatus(ChartVo chartVo) throws Exception {
		
		List <ChartVo> chartData = sql_ma.selectList(namespace + "getAllDvcStatus", chartVo);
		
		System.out.println(chartData);
		List list = new ArrayList();
		for(int i = 0; i < chartData.size(); i++){
			Map map = new HashMap();
			map.put("adt_id", chartData.get(i).getAdtId());
			map.put("startDateTime", chartData.get(i).getStartDateTime());
			map.put("endDateTime", chartData.get(i).getEndDateTime());
			map.put("chartStatus", chartData.get(i).getChartStatus());
			map.put("alarm", chartData.get(i).getAlarm());
			map.put("spdLoad", chartData.get(i).getSpdLoad());
			map.put("spdActualSpeed", chartData.get(i).getSpdActualSpeed());
			map.put("feedOverride", chartData.get(i).getFeedOverride());
			map.put("actualFeed", chartData.get(i).getActualFeed()); 
			map.put("date", chartData.get(i).getDate());
			map.put("name", chartData.get(i).getName());
			map.put("dvcId", chartData.get(i).getDvcId());
			
			list.add(map);
		};
		
		String str = "";
		
		Map resultMap = new HashMap();
		resultMap.put("chartStatus", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}

	@Override
	public String getAllDvcId() throws Exception {
	
		List <ChartVo >dvcIdList = sql_ma.selectList(namespace + "getAllDvcId");
		
		List list = new ArrayList();
		
		for(int i = 0; i < dvcIdList.size();i++){
			Map map = new HashMap();
			map.put("dvcId", dvcIdList.get(i).getId());
			map.put("name", URLEncoder.encode(dvcIdList.get(i).getName(),"utf-8"));
			map.put("status", dvcIdList.get(i).getStatus());
			map.put("operationTime", dvcIdList.get(i).getIncycleTime());
			map.put("opRatio", dvcIdList.get(i).getOpRatio());
			
			String jig = "";
			if(dvcIdList.get(i).getJig()!=null){
				jig = URLEncoder.encode(dvcIdList.get(i).getJig(), "UTF-8");
				jig = jig.replaceAll("\\+", "%20");
			};
			
			String wc = "";
			if(dvcIdList.get(i).getWC()!=null){
				wc = URLEncoder.encode(dvcIdList.get(i).getWC(), "UTF-8");
				wc = wc.replaceAll("\\+", "%20");
			};
			
			map.put("jig", jig);
			map.put("WC", wc);

			list.add(map);
		};
		
		Map resultMap = new HashMap();
		resultMap.put("dvcId", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		
		
		return str;
	}
	
	@Override
	public String polling1() throws Exception {
		return (String) sql_ma.selectOne(namespace + "getVideoMachine");
	};

	@Override
	public String setVideo(String id) throws Exception {
		String rtn = "";
		
		try{
			sql_ma.update(namespace + "setVideo", id);
			rtn = "success";
		}catch(Exception e){
			rtn = "fail";
		};
		return rtn;
	};

	@Override
	public void initVideoPolling() throws Exception {
		sql_ma.update(namespace + "initVideoPolling");
	};

	@Override
	public void initPiePolling() throws Exception {
		sql_ma.update(namespace + "initPiePolling");
	}

	@Override
	public String piePolling1() throws Exception {
		
		return (String) sql_ma.selectOne(namespace + "piePolling1");
	};

	@Override
	public String piePolling2() throws Exception {
		return (String) sql_ma.selectOne(namespace + "piePolling2");
	};

	@Override
	public String setChart1(String id) throws Exception {
		String rtn = "";
		
		try{
			sql_ma.update(namespace +"setChart1", id);
			rtn = "success";
		}catch(Exception e){
			rtn = "fail";
		}
		return rtn;
	};

	@Override
	public String setChart2(String id) throws Exception {
		String rtn = "";
		
		try{
			sql_ma.update(namespace +"setChart2", id);
			rtn = "success";
		}catch(Exception e){
			rtn = "fail";
		}
		return rtn;
	}

	@Override
	public void delVideoMachine(String id) throws Exception {
		sql_ma.update(namespace +"delVideoMachine", id);
	}

	@Override
	public void delChartMachine(String id) throws Exception {
		String machine = (String) sql_ma.selectOne(namespace + "chkChartMachine1", id);

		if(machine==null){
			sql_ma.update(namespace +"delChartMachine2");
		}else{
			sql_ma.update(namespace +"delChartMachine1");
		};
	}

	@Override
	public void test(ChartVo chartVo) throws Exception {

		List <ChartVo> chartData = sql_ma.selectList(namespace + "getAllDvcStatus", chartVo);
	}

	@Override
	public String getAdapterId(ChartVo chartVo) throws Exception {
		List <ChartVo >dvcIdList = sql_ma.selectList(namespace + "getAdapterId", chartVo);
		
		List list = new ArrayList();
		
		for(int i = 0; i < dvcIdList.size();i++){
			Map map = new HashMap();
			map.put("dvcId", dvcIdList.get(i).getDvcId());
			map.put("name", dvcIdList.get(i).getName());
			map.put("chartStatus", dvcIdList.get(i).getChartStatus());
			map.put("operationTime", dvcIdList.get(i).getOperationTime());
			map.put("adtId", dvcIdList.get(i).getAdtId());

			list.add(map);
		};
		
		Map resultMap = new HashMap();
		resultMap.put("dvcId", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		
		return str;
	}

	@Override
	public String getBarChartDvcId() throws Exception {
		List <ChartVo >dvcIdList = sql_ma.selectList(namespace + "getBarChartDvcId");
		
		List list = new ArrayList();
		
		for(int i = 0; i < dvcIdList.size();i++){
			Map map = new HashMap();
			map.put("dvcId", dvcIdList.get(i).getDvcId());
			map.put("name", dvcIdList.get(i).getName());
			map.put("chartStatus", dvcIdList.get(i).getChartStatus());
			map.put("operationTime", dvcIdList.get(i).getOperationTime());
			map.put("jig", URLEncoder.encode(dvcIdList.get(i).getJig(), "UTF-8"));
			map.put("WC", dvcIdList.get(i).getWC());
			map.put("chartOrder", dvcIdList.get(i).getChartOrder());
			map.put("adtId", dvcIdList.get(i).getAdtId());
			
			list.add(map);
		};
		
		Map resultMap = new HashMap();
		resultMap.put("dvcId", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		
		
		return str;
	}


	@Override
	public String showDetailEvent(ChartVo chartVo) throws Exception {
		List <ChartVo> chartData  = sql_ma.selectList(namespace +"showDetailEvent", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < chartData.size(); i++){
			Map map = new HashMap();
			map.put("startDateTime", chartData.get(i).getStartDateTime());
			map.put("endDateTime", chartData.get(i).getEndDateTime());
			map.put("chartStatus", chartData.get(i).getChartStatus());
			map.put("alarm", chartData.get(i).getAlarm());
			map.put("spdLoad", chartData.get(i).getSpdLoad());
			map.put("feedOverride", chartData.get(i).getFeedOverride());
			
			list.add(map);
		};
		
		String str = "";
		Map resultMap = new HashMap();
		resultMap.put("chartData", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		
		return str;
	}


	@Override
	public ChartVo getMachinePosIE(ChartVo chartVo) throws Exception {
		chartVo = (ChartVo) sql_ma.selectOne(namespace + "getMachinePosIE", chartVo);
		return chartVo;
	}


	@Override
	public void setMachinePosIE(ChartVo chartVo) throws Exception {
		sql_ma.update(namespace +"setMachinePosIE", chartVo);
	}


	@Override
	public String getMachineList() throws Exception {
		List <ChartVo> sqlList = sql_ma.selectList(namespace + "getMachineList");
		
		List list = new ArrayList();
		for(int i = 0; i < sqlList.size(); i++){
			Map map = new HashMap();
			map.put("id", sqlList.get(i).getId());
			map.put("name", sqlList.get(i).getName());
			map.put("x", sqlList.get(i).getX());
			map.put("y", sqlList.get(i).getY());
			map.put("pic", sqlList.get(i).getPic());
			map.put("status", sqlList.get(i).getStatus());
			map.put("camIp", sqlList.get(i).getCamIp());
			map.put("company", sqlList.get(i).getCompany());
			map.put("folderId", sqlList.get(i).getFolderId());
			
			list.add(map);
		};
		
		Map resultMap = new HashMap();
		resultMap.put("machineList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	};
	
	@Override
	public String getEmoMachineList(ChartVo chartVo) throws Exception {
		List <ChartVo> sqlList = sql_ma.selectList(namespace + "getEmoMachineList", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < sqlList.size(); i++){
			Map map = new HashMap();
			map.put("id", sqlList.get(i).getId());
			map.put("name", sqlList.get(i).getName());
			map.put("x", sqlList.get(i).getX());
			map.put("y", sqlList.get(i).getY());
			map.put("pic", sqlList.get(i).getPic());
			map.put("status", sqlList.get(i).getStatus());
			map.put("camIp", sqlList.get(i).getCamIp());
			map.put("company", sqlList.get(i).getCompany()); 
			map.put("folderId", sqlList.get(i).getFolderId());
			map.put("video", sqlList.get(i).getVideo());
			map.put("opRatio", sqlList.get(i).getOpRatio());
			map.put("incycleTime", sqlList.get(i).getIncycleTime());
			map.put("waitTime", sqlList.get(i).getWaitTime());
			map.put("alarmTime", sqlList.get(i).getAlarmTime());
			map.put("noconnTime", sqlList.get(i).getNoconnTime());
			
			list.add(map);
		};
		
		Map resultMap = new HashMap();
		resultMap.put("machineList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	};

	@Override
	public void setCardPos(ChartVo chartVo) throws Exception {
		sql_ma.update(namespace + "setCardPos", chartVo);
	}
	
	@Override
	public void setEmoCardPos(ChartVo chartVo) throws Exception {
		sql_ma.update(namespace + "setEmoCardPos", chartVo);
	}

	@Override
	public String makeFolder(ChartVo chartVo) throws Exception {
		sql_ma.insert(namespace + "makeFolder", chartVo);
		String[] splitter = chartVo.getDvcId().split(","); 
		
		String result = "";
		try{
			for(int i = 0; i < splitter.length; i++){
				chartVo.setId(Integer.parseInt(splitter[i]));
				addFolderid(chartVo);
			};
			result = "success";
		}catch(Exception e){
			result = "fail";
		};
		
		return String.valueOf(chartVo.getFolderId());
	}


	@Override
	public String getFolderList(ChartVo chartVo) throws Exception {
		List <ChartVo> folderList = sql_ma.selectList(namespace + "getFolderList", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < folderList.size(); i++){
			Map map = new HashMap();
			map.put("folderId", folderList.get(i).getFolderId());
			map.put("name", folderList.get(i).getName());
			map.put("x", folderList.get(i).getX());
			map.put("y", folderList.get(i).getY());
			map.put("pic", folderList.get(i).getPic());
			
			list.add(map);
		};
		
		Map folderMap = new HashMap();
		folderMap.put("folderList", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(folderMap);
		return str;
	}


	@Override
	public String addFolderid(ChartVo chartVo) throws Exception {
		String result = "";
		try{
			sql_ma.update(namespace + "addFolderDvc", chartVo);
			result = "success";
		}catch(Exception e){
			
			result = "fail";
		};
		
		return result;
	}


	@Override
	public String getCardsList(ChartVo chartVo) throws Exception {
		List <ChartVo> sqlList = sql_ma.selectList(namespace + "getCardsList", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < sqlList.size(); i++){
			Map map = new HashMap();
			map.put("id", sqlList.get(i).getId());
			map.put("name", sqlList.get(i).getName());
			map.put("x", sqlList.get(i).getX());
			map.put("y", sqlList.get(i).getY());
			map.put("pic", sqlList.get(i).getPic());
			map.put("status", sqlList.get(i).getStatus());
			map.put("camIp", sqlList.get(i).getCamIp());
			map.put("company", sqlList.get(i).getCompany());
			map.put("folderId", sqlList.get(i).getFolderId());
			
			list.add(map);
		};
		
		Map resultMap = new HashMap();
		resultMap.put("machineList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(resultMap);
		return str;
	}


	@Override
	public void removeCard(ChartVo chartVo) throws Exception {
		sql_ma.update(namespace + "removeCard", chartVo);
	}


	@Override
	public void modifyFolderName(ChartVo chartVo) throws Exception {
		sql_ma.update(namespace + "modifyFolderName", chartVo);
	}


	@Override
	public void modifyMachineName(ChartVo chartVo) throws Exception {
		sql_ma.update(namespace + "modifyMachineName", chartVo);
	}


	@Override
	public ChartVo getMachinePos(ChartVo chartVo) throws Exception {
		chartVo = (ChartVo) sql_ma.selectOne(namespace + "getMachinePos", chartVo);
		return chartVo;
	}


	@Override
	public String getAllMachine() throws Exception {
		List<ChartVo> machineList = sql_ma.selectList(namespace + "getAllMachine");
		
		List list = new ArrayList();
		
		for(int i = 0; i < machineList.size(); i++){
			Map map = new HashMap();
			map.put("id", machineList.get(i).getId());
			map.put("name", URLEncoder.encode(machineList.get(i).getName(), "utf-8"));
			map.put("pic", machineList.get(i).getPic());
			map.put("x", machineList.get(i).getX());
			map.put("y", machineList.get(i).getY());
			map.put("w", machineList.get(i).getW());
			map.put("display", machineList.get(i).getDisplay());
			map.put("status", machineList.get(i).getChartStatus());
			map.put("rotate", machineList.get(i).getRotate());
			map.put("isEmpty", machineList.get(i).getIsEmpty());
			map.put("isEmpty", machineList.get(i).getIsEmpty());
			map.put("endFive", machineList.get(i).getEndFive());
			map.put("endThree", machineList.get(i).getEndThree());
			
			list.add(map);
		};
		
		Map machineMap = new HashMap();
		machineMap.put("machineList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		
		str = om.defaultPrettyPrintingWriter().writeValueAsString(machineMap);
		
		return str;
	}


	@Override
	public String getAllBlockMachine() throws Exception {
		List<ChartVo> machineList = sql_ma.selectList(namespace + "getAllBlockMachine");
		
		List list = new ArrayList();
		
		for(int i = 0; i < machineList.size(); i++){
			Map map = new HashMap();
			map.put("id", machineList.get(i).getId());
			map.put("name", machineList.get(i).getName());
			map.put("pic", machineList.get(i).getPic());
			map.put("x", machineList.get(i).getX());
			map.put("y", machineList.get(i).getY());
			map.put("w", machineList.get(i).getW());
			map.put("display", machineList.get(i).getDisplay());
			map.put("status", machineList.get(i).getChartStatus());
			map.put("rotate", machineList.get(i).getRotate());
			map.put("isEmpty", machineList.get(i).getIsEmpty());
			map.put("endFive", machineList.get(i).getEndFive());
			map.put("endThree", machineList.get(i).getEndThree());
			
			list.add(map);
		};
		
		Map machineMap = new HashMap();
		machineMap.put("machineList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		
		str = om.defaultPrettyPrintingWriter().writeValueAsString(machineMap);
		
		return str;
	}

	
	@Override
	public void setInitCardPos(ChartVo chartVo) throws Exception {
		try{
			sql_ma.update(namespace + "setInitCardPos", chartVo);
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	@Override
	public String getReportBarData(ChartVo chartVo) throws Exception {
		
		List<ChartVo> listData = sql_ma.selectList(namespace + "getReportBarData", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < listData.size(); i++){
			Map map = new HashMap();
			map.put("name", listData.get(i).getName());
			map.put("incycleTime", listData.get(i).getIncycleTime());
			map.put("waitTime", listData.get(i).getWaitTime());
			map.put("alarmTime", listData.get(i).getAlarmTime());
			map.put("noconnTime", listData.get(i).getNoconnTime());
			map.put("opRatio", listData.get(i).getOpRatio());
			map.put("cuttingTime", listData.get(i).getCuttingTime());
			
			list.add(map);
		};
		
		String str = "";
		Map machineMap = new HashMap();
		machineMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(machineMap);
		
		return str;
	}


	@Override
	public String getMachineStatus(ChartVo chartVo) throws Exception {
		List<ChartVo> machineList = sql_ma.selectList(namespace + "getMachineStatus", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < machineList.size(); i++){
			Map map = new HashMap();
			map.put("id", machineList.get(i).getId());
			map.put("name", URLEncoder.encode(machineList.get(i).getName(), "utf-8"));
			map.put("pic", machineList.get(i).getPic());
			map.put("x", machineList.get(i).getX());
			map.put("y", machineList.get(i).getY());
			map.put("w", machineList.get(i).getW());
			map.put("display", machineList.get(i).getDisplay());
			map.put("status", machineList.get(i).getChartStatus());
			map.put("rotate", machineList.get(i).getRotate());
			map.put("isEmpty", machineList.get(i).getIsEmpty());
			map.put("endFive", machineList.get(i).getEndFive());
			map.put("endThree", machineList.get(i).getEndThree());
			
			list.add(map);
		};
		
		String str = "";
		Map machineMap = new HashMap();
		machineMap.put("machineList", list);
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(machineMap);
		
		return str;
	}


	@Override
	public ChartVo getDvcData(ChartVo chartVo) throws Exception {
		chartVo = (ChartVo) sql_ma.selectOne(namespace + "getDvcData", chartVo);
		return chartVo;
	}


	@Override
	public ChartVo getDvcTemper(ChartVo chartVo) throws Exception {
		chartVo = (ChartVo) sql_ma.selectOne(namespace + "getDvcTemper", chartVo);
		return chartVo;
	}


	@Override
	public String getTimeData(ChartVo chartVo) throws Exception {
		List <ChartVo> statusList = sql_ma.selectList(namespace +"getTimeData", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < statusList.size(); i++){
			Map map = new HashMap();
			map.put("dvcId", statusList.get(i).getDvcId());
			map.put("status", statusList.get(i).getStatus());
			
			list.add(map);
		};
		
		Map statusMap = new HashMap();
		statusMap.put("statusList", list);
		
		ObjectMapper om = new ObjectMapper();
		String str = "";
		str = om.defaultPrettyPrintingWriter().writeValueAsString(statusMap);
		return str;
	}


	@Override 
	public String getMachineName(ChartVo chartVo) throws Exception {
		String str = (String) sql_ma.selectOne(namespace + "getMachineName", chartVo);
		return URLEncoder.encode(str, "utf-8");
	}


	@Override
	public String getLeftTableData(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = sql_ma.selectList(namespace + "getLeftTableData", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < dataList.size(); i++ ){
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("name",URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("programName", dataList.get(i).getLastProgramName());
			map.put("opRatio", dataList.get(i).getOpRatio());
			map.put("inCycleTime", dataList.get(i).getIncycleTime());
			map.put("noconnTime", dataList.get(i).getNoconnTime());
			
			list.add(map);
		};
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("tableDataList", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}


	@Override
	public ChartVo getMachineData(ChartVo chartVo) throws Exception {
		chartVo = (ChartVo) sql_ma.selectOne(namespace + "getMachineData", chartVo);
		return chartVo;
	}


	@Override
	public String getStatistic(ChartVo chartVo) throws Exception {
		
		List<ChartVo> dataList = sql_ma.selectList(namespace + "getStatistic", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < dataList.size(); i++ ){
			Map map = new HashMap();
			map.put("id", dataList.get(i).getId());
			map.put("name",URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("programName", dataList.get(i).getLastProgramName());
			map.put("opRatio", dataList.get(i).getOpRatio());
			map.put("cycleTime", dataList.get(i).getCycleTime());
			map.put("inCycleTime", dataList.get(i).getIncycleTime());
			
			list.add(map);
		};
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}


	@Override
	public String getLeftRailChart(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = sql_ma.selectList(namespace + "getLeftRailChart", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < dataList.size(); i++ ){
			Map map = new HashMap();
			map.put("name",URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("hyung", dataList.get(i).getHyung());
			map.put("dvcId", dataList.get(i).getId());
			map.put("tgCnt", dataList.get(i).getTgCnt());
			map.put("fnCnt", dataList.get(i).getFnCnt());
			map.put("waitingTime", dataList.get(i).getWaitingTime());
			map.put("isEmpty", dataList.get(i).getIsEmpty());
			
			list.add(map);
		};
		
		String str = ""; 
		Map listMap = new HashMap();
		listMap.put("chartList", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getLeftBlockChart() throws Exception {
		List<ChartVo> dataList = sql_ma.selectList(namespace + "getLeftBlockChart");
		
		List list = new ArrayList();
		for(int i = 0; i < dataList.size(); i++ ){
			Map map = new HashMap();
			map.put("name",URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("hyung", dataList.get(i).getHyung());
			map.put("dvcId", dataList.get(i).getId());
			map.put("tgCnt", dataList.get(i).getTgCnt());
			map.put("fnCnt", dataList.get(i).getFnCnt());
			map.put("waitingTime", dataList.get(i).getWaitingTime());
			map.put("isEmpty", dataList.get(i).getIsEmpty());
			
			list.add(map);
		};
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("chartList", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getDetailRailData(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = sql_ma.selectList(namespace + "getDetailRailData", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < dataList.size(); i++ ){
			Map map = new HashMap();
			map.put("lastAlarmCode", dataList.get(i).getLastAlarmCode());
			map.put("lastAlarmMsg", dataList.get(i).getLastAlarmMsg());
			map.put("lastTgPrdctNum", dataList.get(i).getLastTgPrdctNum());
			map.put("lastFnPrdctNum", dataList.get(i).getLastFnPrdctNum());
			map.put("railSize", dataList.get(i).getRailSize());
			map.put("tgCnt", dataList.get(i).getTgCnt());
			map.put("remainCnt", dataList.get(i).getRemainCnt());
			map.put("stoneX", dataList.get(i).getStoneX());
			map.put("stoneY", dataList.get(i).getStoneY());
			map.put("stoneZ", dataList.get(i).getStoneZ());
			map.put("stoneB", dataList.get(i).getStoneB());
			map.put("ncAlarmNum1", dataList.get(i).getNcAlarmNum1());
			map.put("ncAlarmMsg1", URLEncoder.encode(dataList.get(i).getNcAlarmMsg1(),"utf-8"));
			map.put("ncAlarmNum2", dataList.get(i).getNcAlarmNum2());
			map.put("ncAlarmMsg2", URLEncoder.encode(dataList.get(i).getNcAlarmMsg2(),"utf-8"));
			map.put("ncAlarmNum3", dataList.get(i).getNcAlarmNum3());
			map.put("ncAlarmMsg3", URLEncoder.encode(dataList.get(i).getNcAlarmMsg3(),"utf-8"));
			map.put("ncAlarmNum4", dataList.get(i).getNcAlarmNum4());
			map.put("ncAlarmMsg4", URLEncoder.encode(dataList.get(i).getNcAlarmMsg4(),"utf-8"));
			map.put("ncAlarmNum5", dataList.get(i).getNcAlarmNum5());
			map.put("ncAlarmMsg5", URLEncoder.encode(dataList.get(i).getNcAlarmMsg5(),"utf-8"));
			map.put("pmcAlarmNum1", dataList.get(i).getPmcAlarmNum1());
			map.put("pmcAlarmMsg1", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg1(),"utf-8"));
			map.put("pmcAlarmNum2", dataList.get(i).getPmcAlarmNum2());
			map.put("pmcAlarmMsg2", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg2(),"utf-8"));
			map.put("pmcAlarmNum3", dataList.get(i).getPmcAlarmNum3());
			map.put("pmcAlarmMsg3", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg3(),"utf-8"));
			map.put("pmcAlarmNum4", dataList.get(i).getPmcAlarmNum4());
			map.put("pmcAlarmMsg4", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg4(),"utf-8"));
			map.put("pmcAlarmNum5", dataList.get(i).getPmcAlarmNum5());
			map.put("pmcAlarmMsg5", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg5(),"utf-8"));
			map.put("LastAvrCycleTime", dataList.get(i).getLastAvrCycleTime());
			
			list.add(map);
		};
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}

	@Override
	public String getDetailBlockData(ChartVo chartVo) throws Exception {
		List<ChartVo> dataList = sql_ma.selectList(namespace + "getDetailBlockData", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < dataList.size(); i++ ){
			Map map = new HashMap();
			map.put("lastAlarmCode", dataList.get(i).getLastAlarmCode());
			map.put("lastAlarmMsg", dataList.get(i).getLastAlarmMsg());
			map.put("lastTgPrdctNum", dataList.get(i).getLastTgPrdctNum());
			map.put("lastFnPrdctNum", dataList.get(i).getLastFnPrdctNum());
			map.put("tgCnt", dataList.get(i).getTgCnt());
			map.put("remainCnt", dataList.get(i).getRemainCnt());
			map.put("ncAlarmNum1", dataList.get(i).getNcAlarmNum1());
			map.put("ncAlarmMsg1", URLEncoder.encode(dataList.get(i).getNcAlarmMsg1(),"utf-8"));
			map.put("ncAlarmNum2", dataList.get(i).getNcAlarmNum2());
			map.put("ncAlarmMsg2", URLEncoder.encode(dataList.get(i).getNcAlarmMsg2(),"utf-8"));
			map.put("ncAlarmNum3", dataList.get(i).getNcAlarmNum3());
			map.put("ncAlarmMsg3", URLEncoder.encode(dataList.get(i).getNcAlarmMsg3(),"utf-8"));
			map.put("ncAlarmNum4", dataList.get(i).getNcAlarmNum4());
			map.put("ncAlarmMsg4", URLEncoder.encode(dataList.get(i).getNcAlarmMsg4(),"utf-8"));
			map.put("ncAlarmNum5", dataList.get(i).getNcAlarmNum5());
			map.put("ncAlarmMsg5", URLEncoder.encode(dataList.get(i).getNcAlarmMsg5(),"utf-8"));
			map.put("pmcAlarmNum1", dataList.get(i).getPmcAlarmNum1());
			map.put("pmcAlarmMsg1", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg1(),"utf-8"));
			map.put("pmcAlarmNum2", dataList.get(i).getPmcAlarmNum2());
			map.put("pmcAlarmMsg2", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg2(),"utf-8"));
			map.put("pmcAlarmNum3", dataList.get(i).getPmcAlarmNum3());
			map.put("pmcAlarmMsg3", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg3(),"utf-8"));
			map.put("pmcAlarmNum4", dataList.get(i).getPmcAlarmNum4());
			map.put("pmcAlarmMsg4", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg4(),"utf-8"));
			map.put("pmcAlarmNum5", dataList.get(i).getPmcAlarmNum5());
			map.put("pmcAlarmMsg5", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg5(),"utf-8"));
			map.put("toolNum1", dataList.get(i).getToolNum1());
			map.put("toolNum2", dataList.get(i).getToolNum2());
			map.put("toolNum3", dataList.get(i).getToolNum3());
			map.put("toolNum4", dataList.get(i).getToolNum4());
			map.put("toolNum5", dataList.get(i).getToolNum5());
			map.put("toolNum6", dataList.get(i).getToolNum6());
			map.put("toolNum7", dataList.get(i).getToolNum7());
			map.put("toolNum8", dataList.get(i).getToolNum8());
			map.put("toolNum9", dataList.get(i).getToolNum9());
			map.put("toolNum10", dataList.get(i).getToolNum10());
			map.put("toolNum11", dataList.get(i).getToolNum11());
			map.put("toolNum12", dataList.get(i).getToolNum12());
			map.put("toolTime1", dataList.get(i).getToolTime1());
			map.put("toolTime2", dataList.get(i).getToolTime2());
			map.put("toolTime3", dataList.get(i).getToolTime3());
			map.put("toolTime4", dataList.get(i).getToolTime4());
			map.put("toolTime5", dataList.get(i).getToolTime5());
			map.put("toolTime6", dataList.get(i).getToolTime6());
			map.put("toolTime7", dataList.get(i).getToolTime7());
			map.put("toolTime8", dataList.get(i).getToolTime8());
			map.put("toolTime9", dataList.get(i).getToolTime9());
			map.put("toolTime10", dataList.get(i).getToolTime10());
			map.put("toolTime11", dataList.get(i).getToolTime11());
			map.put("toolTime12", dataList.get(i).getToolTime12());
			map.put("LastAvrCycleTime", dataList.get(i).getLastAvrCycleTime());
			
			list.add(map);
		};
		
		String str = "";
		Map listMap = new HashMap();
		listMap.put("dataList", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(listMap);
		return str;
	}
	

	@Override
	public ChartVo getMcNo(ChartVo chartVo) throws Exception {
		chartVo = (ChartVo) sql_ma.selectOne(namespace + "getMcNo", chartVo);
		return chartVo;
	}


	@Override
	public String getReportData(ChartVo chartVo) throws Exception {
		List <ChartVo> dataList = sql_ma.selectList(namespace + "getReportData", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < dataList.size(); i++){
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("workDate", dataList.get(i).getWorkDate());
			map.put("hyung", dataList.get(i).getHyung());
			map.put("avrCycleTimeSec", dataList.get(i).getAvrCycleTimeSec());
			map.put("tgPrdctCnt", dataList.get(i).getTgPrdctCnt());
			map.put("railSize", dataList.get(i).getRailSize());
			map.put("prdctCnt", dataList.get(i).getPrdctCnt());
			map.put("cycleCnt", dataList.get(i).getCycleCnt());
			map.put("crtRunningTimeSec", dataList.get(i).getCrtRunningTimeSec());
			map.put("downTimeSec", dataList.get(i).getDownTimeSec());
			map.put("runningRatio", dataList.get(i).getRunningRatio());
			map.put("alarmCnt", dataList.get(i).getAlarmCnt());
			map.put("lastDownTime", dataList.get(i).getLastDownTime());
			
			list.add(map);
		};
		
		String str = "";
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}


	@Override
	public String getTargetBlockData(ChartVo chartVo) throws Exception {
		List <ChartVo> dataList = sql_ma.selectList(namespace + "getTargetBlockData", chartVo);
		
		List list = new ArrayList(); 
		for(int i = 0; i < dataList.size(); i++){
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("tgCnt", dataList.get(i).getTgCnt());
			map.put("isEmpty", dataList.get(i).getIsEmpty());
			
			list.add(map);
		};
		
		String str = "";
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}

	@Override
	public String getTargetData(ChartVo chartVo) throws Exception {
		List <ChartVo> dataList = sql_ma.selectList(namespace + "getTargetData", chartVo);
		
		List list = new ArrayList(); 
		for(int i = 0; i < dataList.size(); i++){
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(), "utf-8"));
			map.put("tgCnt", dataList.get(i).getTgCnt());
			map.put("isEmpty", dataList.get(i).getIsEmpty());
			
			list.add(map);
		};
		
		String str = "";
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}
	

	@Override
	public String addTargetCnt(ChartVo chartVo) throws Exception {
		String str = ""; 
		try{
			sql_ma.insert(namespace + "addTargetCnt", chartVo);
			str = "success";
		}catch(Exception e){
			str = "fail";
			e.printStackTrace();
			
		};
		
		return str;
	}

	@Override
	public String addBlockTargetCnt(ChartVo chartVo) throws Exception {
		String str = ""; 
		try{
			sql_ma.insert(namespace + "addBlockTargetCnt", chartVo);
			str = "success";
		}catch(Exception e){
			str = "fail";
			e.printStackTrace();
			
		};
		
		return str;
	}

	@Override
	public String addMonthlyRailTarget(ChartVo chartVo) throws Exception {
		String  str = "";
		try{
			sql_ma.insert(namespace + "addMonthlyRailTarget", chartVo);
			str = "success";
		}catch(Exception e){
			str = "fail";
			e.printStackTrace();
		}
		return str;
	}

	@Override
	public String addMonthlyBlockTarget(ChartVo chartVo) throws Exception {
		String  str = "";
		try{
			sql_ma.insert(namespace + "addMonthlyBlockTarget", chartVo);
			str = "success"; 
		}catch(Exception e){
			str = "fail";
			e.printStackTrace();
		}
		return str;
	}
	

	@Override
	public String getMonthlyRailTatget() throws Exception {
		String tgCnt = (String) sql_ma.selectOne(namespace + "getMonthlyRailTatget");
		return tgCnt;
	}


	@Override
	public String getMonthlyBlockTatget() throws Exception {
		String tgCnt = (String) sql_ma.selectOne(namespace + "getMonthlyBlockTatget");
		return tgCnt;
	}


	@Override
	public String getAlarmList(ChartVo chartVo) throws Exception {
List <ChartVo> dataList = sql_ma.selectList(namespace + "getAlarmList", chartVo);
		
		List list = new ArrayList(); 
		for(int i = 0; i < dataList.size(); i++){
			Map map = new HashMap();
			map.put("startDateTime", dataList.get(i).getStartDateTime());
			map.put("endDateTime", dataList.get(i).getEndDateTime());
			map.put("ncAlarmNum1", dataList.get(i).getNcAlarmNum1());
			map.put("ncAlarmMsg1", URLEncoder.encode(dataList.get(i).getNcAlarmMsg1(),"utf-8"));
			map.put("ncAlarmNum2", dataList.get(i).getNcAlarmNum2());
			map.put("ncAlarmMsg2", URLEncoder.encode(dataList.get(i).getNcAlarmMsg2(),"utf-8"));
			map.put("ncAlarmNum3", dataList.get(i).getNcAlarmNum3());
			map.put("ncAlarmMsg3", URLEncoder.encode(dataList.get(i).getNcAlarmMsg3(),"utf-8"));
			map.put("ncAlarmNum4", dataList.get(i).getNcAlarmNum4());
			map.put("ncAlarmMsg4", URLEncoder.encode(dataList.get(i).getNcAlarmMsg4(),"utf-8"));
			map.put("ncAlarmNum5", dataList.get(i).getNcAlarmNum5());
			map.put("ncAlarmMsg5", URLEncoder.encode(dataList.get(i).getNcAlarmMsg5(),"utf-8"));
			map.put("pmcAlarmNum1", dataList.get(i).getPmcAlarmNum1());
			map.put("pmcAlarmMsg1", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg1(),"utf-8"));
			map.put("pmcAlarmNum2", dataList.get(i).getPmcAlarmNum2());
			map.put("pmcAlarmMsg2", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg2(),"utf-8"));
			map.put("pmcAlarmNum3", dataList.get(i).getPmcAlarmNum3());
			map.put("pmcAlarmMsg3", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg3(),"utf-8"));
			map.put("pmcAlarmNum4", dataList.get(i).getPmcAlarmNum4());
			map.put("pmcAlarmMsg4", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg4(),"utf-8"));
			map.put("pmcAlarmNum5", dataList.get(i).getPmcAlarmNum5());
			map.put("pmcAlarmMsg5", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg5(),"utf-8"));
			
			list.add(map);
		};
		
		String str = "";
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}


	@Override
	public String getStartTime(ChartVo chartVo) throws Exception {
		String str = (String) sql_ma.selectOne(namespace + "getStartTime", chartVo);
		return str;
	}


	@Override
	public String getAllEvt(ChartVo chartVo) throws Exception {
		List <ChartVo> dataList = sql_ma.selectList(namespace + "getAllEvt", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < dataList.size(); i++){
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(),"utf-8"));
			map.put("status", dataList.get(i).getStatus());
			map.put("type", dataList.get(i).getType());
			map.put("startDateTime", dataList.get(i).getStartDateTime());
			map.put("ncAlarmNum1", dataList.get(i).getNcAlarmNum1());
			map.put("ncAlarmMsg1", URLEncoder.encode(dataList.get(i).getNcAlarmMsg1(),"utf-8"));
			map.put("ncAlarmNum2", dataList.get(i).getNcAlarmNum2());
			map.put("ncAlarmMsg2", URLEncoder.encode(dataList.get(i).getNcAlarmMsg2(),"utf-8"));
			map.put("ncAlarmNum3", dataList.get(i).getNcAlarmNum3());
			map.put("ncAlarmMsg3", URLEncoder.encode(dataList.get(i).getNcAlarmMsg3(),"utf-8"));
			map.put("ncAlarmNum4", dataList.get(i).getNcAlarmNum4());
			map.put("ncAlarmMsg4", URLEncoder.encode(dataList.get(i).getNcAlarmMsg4(),"utf-8"));
			map.put("ncAlarmNum5", dataList.get(i).getNcAlarmNum5());
			map.put("ncAlarmMsg5", URLEncoder.encode(dataList.get(i).getNcAlarmMsg5(),"utf-8"));
			map.put("pmcAlarmNum1", dataList.get(i).getPmcAlarmNum1());
			map.put("pmcAlarmMsg1", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg1(),"utf-8"));
			map.put("pmcAlarmNum2", dataList.get(i).getPmcAlarmNum2());
			map.put("pmcAlarmMsg2", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg2(),"utf-8"));
			map.put("pmcAlarmNum3", dataList.get(i).getPmcAlarmNum3());
			map.put("pmcAlarmMsg3", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg3(),"utf-8"));
			map.put("pmcAlarmNum4", dataList.get(i).getPmcAlarmNum4());
			map.put("pmcAlarmMsg4", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg4(),"utf-8"));
			map.put("pmcAlarmNum5", dataList.get(i).getPmcAlarmNum5());
			map.put("pmcAlarmMsg5", URLEncoder.encode(dataList.get(i).getPmcAlarmMsg5(),"utf-8"));
			
			list.add(map);
		};
		
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}


	@Override
	public String getTotalOpRatio(ChartVo chartVo) throws Exception {
		List <ChartVo> dataList = sql_ma.selectList(namespace + "getTotalOpRatio", chartVo);
		
		List list = new ArrayList();
		
		for(int i = 0;  i < dataList.size(); i++){
			Map map = new HashMap();
			map.put("opRatio", dataList.get(i).getOpRatio()); 
			
			list.add(map);
		};
		
		String str = "";
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}


	@Override
	public String getAllDvcList() throws Exception {
		List <ChartVo> dataList = sql_ma.selectList(namespace + "getAllDvcList");
		
		List list = new ArrayList();
		
		for(int i = 0;  i < dataList.size(); i++){
			Map map = new HashMap();
			map.put("dvcId", dataList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(dataList.get(i).getName(),"utf-8"));
			
			list.add(map);
		};
		
		String str = "";
		Map dataMap = new HashMap();
		dataMap.put("dataList", list);
		ObjectMapper om = new ObjectMapper();
		
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dataMap);
		return str;
	}


	@Override
	public ChartVo getLastConnection(ChartVo chartVo) throws Exception {
		chartVo = (ChartVo) sql_ma.selectOne(namespace + "getLastConnection", chartVo);
		return chartVo;
	}

};