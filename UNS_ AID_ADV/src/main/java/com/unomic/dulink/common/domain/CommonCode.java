package com.unomic.dulink.common.domain;

import java.util.HashMap;
import java.util.Map;

public class CommonCode {
	//public static String myApiKey = "AIzaSyA0hVpizUS0REO6Xxu0EhYqNR8owmkVBww";
	//public static String myApiKey = "AIzaSyA2X_BiUXtFdM-nbCPgcPq88kbzagmP50Q";
	
	//public static boolean isTest = true;
	public static boolean isTest = false;
	
	public static String UTC_ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	public static String DATE_TIME_FORMAT_SEC = "yyyy-MM-dd HH:mm:ss";
	public static String DATE_FORMAT = "yyyy-MM-dd";

	public static String myApiKey 							= "AIzaSyBeRxeV7CK4AibPFMTVGmBDd9XTwB3jc5Q";
	public static final String SERVER_IP 					= "106.240.234.114:8080";
	public static final String POST_URL 					= "/DULink/nfc/adapterPost.do";
	public static final String FILE_EXTEND 					= ".txt";
	
	//public static final String TIME_ZONE					= "Europe/Rome";
	public static final String TIME_ZONE					= "Asia/Seoul";
	public static final String UTC_TIME_ZONE				= "UTC";
	
	//TARGET = "client"  is CNC Agent.
	//TARGET = "server" is UNOMIC Agent.
	public static final String TARGET 						= "server";
	
	
	// security cd
	public static String CODE_DVC_IOS						= "01300001";
	public static String CODE_DVC_ANDROID					= "01300002";
	
	public static String CODE_GCM_SUCCESS					= "01400001";
	public static String CODE_GCM_FAIL						= "01400002";
	//public static String CODE_GCM_FAIL_EMPTY_USER			= "01400002";
	//public static String CODE_GCM_FAIL_UNREG_USER			= "01400003";
	
	public static String MSG_IN_CYCLE						= "IN-CYCLE";
	public static String MSG_WAIT					  		= "WAIT";
	public static String MSG_ALARM							= "ALARM";
	public static String MSG_NO_CONNECTION				 	= "NO-CONNECTION";
	
	public static String MSG_DATE_START						= "DATE_START";
	public static String MSG_STAT_INIT						= "STAT_INIT";

	public static String MSG_HOUR_STD						= "18";
	public static String MSG_MIN_STD						= "00";
	
	public static String MSG_HOUR_STD_UTC					= "11";
	
	public static String MSG_SDF_STD						= DATE_FORMAT+" "+MSG_HOUR_STD+":"+MSG_MIN_STD+":00.000";
	public static String MSG_SDF_STD_UTC					= DATE_FORMAT+"'T'"+MSG_HOUR_STD_UTC+":"+MSG_MIN_STD+":00.000'Z'";
	public static String MSG_UNAVAIL						= "UNAVAILABLE";
	public static String MSG_XML_DELIMITER					= "</MTConnectStreams>";
	
	public static String MSG_MC_TYPE_RAIL					= "rail";
	public static String MSG_MC_TYPE_BLOCK					= "block";
	public static String MSG_RTN_ERROR_PARSE_FAILED			= "Data Parsing Failed";
	
	public static String MSG_PRGM_TYPE_MAIN					= "main";
	public static String MSG_PRGM_TYPE_PALLET_CHAGE			= "plt_chg";
	public static String MSG_PRGM_END_ON					= "ON";
	public static String MSG_PRGM_END_OFF					= "OFF";
	
	
	public static Integer CONNECT_TIMEOUT					= 3000;
	public static Integer READ_TIMEOUT						= 3000;
	public static Integer IOL_STATUS_LENGTH					= 4;
	
	public static String MSG_ADT_STATUS_SEND_REQ			= "SEND_REQ";
	public static String MSG_ADT_STATUS_NO_REQ				= "NO_REQ";
	
	public static String MSG_HTTP							= "http://";
	public static String MSG_AGT_URL						= "/current";
	public static String MSG_IOL_URL						= "/getParam.cgi?DIStatus_00=?&DIStatus_01=?&DIStatus_02=?&DIStatus_03=?";
 
	public static final Map<String , String> MAP_MSG_APC_PRGM = new HashMap<String , String>() {{
	    put("1", "O9921");
	    put("6", "O101");
	    put("7", "O9921");
	    put("8", "O9921");
	    put("20","O0001");
	    put("25", "O9921");
	    put("26", "O9921");
	    put("27", "O9921");
	}};
	
}

