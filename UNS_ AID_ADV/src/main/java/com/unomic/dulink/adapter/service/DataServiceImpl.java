package com.unomic.dulink.adapter.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.session.SqlSession;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.unomic.dulink.adapter.domain.AlarmVO;
import com.unomic.dulink.adapter.domain.FrequentVO;
import com.unomic.dulink.adapter.domain.LampVO;
import com.unomic.dulink.adapter.domain.RareVO;
import com.unomic.dulink.adapter.domain.TokenVO;

// TODO: Auto-generated Javadoc
/**
 * The Class DataServiceImpl.
 */
@Service
@Repository
public class DataServiceImpl implements DataService {

	/** The sql session template. */
	@Autowired
	SqlSession sqlSessionTemplate;

	/** The Constant namespace. */
	private final static String namespace = "com.factory911.adapter.";

	/*
	 * (비Javadoc)
	 * 
	 * @see
	 * com.unomic.dulink.adapter.service.DataService#setRareData(java.lang.String)
	 */
	@Override
	public String setRareData(String inputStrJson) throws Exception {

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		String ip = request.getHeader("X-Forwarded-For");

		if (ip == null) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null) {
			ip = request.getHeader("WL-Proxy-Client-IP"); // 웹로직
		}
		if (ip == null) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null) {
			ip = request.getRemoteAddr();
		}

		long Wholestart = System.currentTimeMillis(); // 시작하는 시점 계산
		String result = "";
		List<RareVO> rareDataList = new ArrayList<RareVO>();
		List<AlarmVO> list = new ArrayList<AlarmVO>();
		List<AlarmVO> exceptionList = new ArrayList<AlarmVO>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String[] ipArray = ip.split("\\.");

		int queryChcker = 0;

		try {

			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = new JSONObject();
			String ipAddress;
			try {
				ipAddress = ipArray[0] + "." + ipArray[1] + "." + ipArray[2];
			} catch (Exception e) {
				ipAddress = "0.0.0.0";
			}

			jsonObject = (JSONObject) jsonParser.parse(inputStrJson);
			JSONArray dataList = (JSONArray) jsonObject.get("data");

			String dvcId = jsonObject.get("dvcId").toString();

			if (dvcId.equals("000")) {
				dvcId = (String) sqlSessionTemplate.selectOne(namespace + "getDvcId", ipAddress);

				sqlSessionTemplate.insert(namespace + "setDevice", dvcId);

				return dvcId;
			}

			String lastStart = (String) sqlSessionTemplate.selectOne(namespace + "getLastRareData", dvcId);
			Date beforeDate = new Date();
			Calendar beforeCal = Calendar.getInstance();
			boolean check = false;
			if (lastStart != null) {
				beforeDate = sdf.parse(lastStart);
				beforeCal = Calendar.getInstance();
				beforeCal.setTime(beforeDate);
				check = true;
			}

			/* String send_date_time = jsonObject.get("send_date_time").toString(); */

			for (int j = 0; j < dataList.size(); j++) {
				JSONObject rareData = (JSONObject) dataList.get(j);
				RareVO rareVO = new RareVO();
				if (j < dataList.size() - 1) {
					JSONObject new_rareData = (JSONObject) dataList.get(j + 1);
					rareVO.setEndDateTime(new_rareData.get("start_date_time").toString());
				}

				rareVO.setDvcId(Integer.parseInt(dvcId));
				rareVO.setStartDateTime(rareData.get("start_date_time").toString());

				rareVO.setMode(rareData.get("mode").toString());
				rareVO.setStatus(rareData.get("status").toString());
				rareVO.setMainPrgmName(rareData.get("main_prg_name").toString());
				rareVO.setMdlT(rareData.get("mdl_t").toString());
				rareVO.setMdlH(rareData.get("mdl_h").toString());
				rareVO.setMdlD(rareData.get("mdl_d").toString());
				rareVO.setMdlM1(rareData.get("mdl_m1").toString());
				rareVO.setMdlM2(rareData.get("mdl_m2").toString());
				rareVO.setMdlM3(rareData.get("mdl_m3").toString());
				rareVO.setPrgmHead(rareData.get("prgm_head").toString());
				rareVO.setPartCount(Integer.parseInt(rareData.get("part_count").toString()));
				rareVO.setTtlPartCount(Integer.parseInt(rareData.get("ttl_part_count").toString()));
				rareVO.setSpdOvrd(Integer.parseInt(rareData.get("spd_ovrd").toString()));
				rareVO.setFdOvrd(Integer.parseInt(rareData.get("fd_ovrd").toString()));
				rareVO.setPmcStl(Integer.parseInt(rareData.get("pmc_stl").toString()));
				rareVO.setPmcCut(Integer.parseInt(rareData.get("pmc_cut").toString()));
				rareVO.setPmcSingleBlock(Integer.parseInt(rareData.get("pmc_single_block").toString()));
				rareVO.setPmcFeedHold(Integer.parseInt(rareData.get("pmc_feed_hold").toString()));
				rareVO.setPmcEmg(Integer.parseInt(rareData.get("pmc_emg").toString()));
				rareVO.setAlarm(rareData.get("alarm").toString());

				if (rareVO.getPmcEmg() == 0 || rareVO.getPmcSingleBlock() == 1 || rareVO.getPmcFeedHold() == 1) {
					if (exceptionList.size() == 0 && queryChcker == 0) {
						exceptionList = sqlSessionTemplate.selectList(namespace + "getAlarmExceptionList",
								rareVO.getDvcId());
						queryChcker = 1;
					}

					JSONParser jsonParser1 = new JSONParser();
					JSONArray dataList1 = (JSONArray) jsonParser1.parse(rareVO.getAlarm());

					int alarmCheck = 0;
					List<AlarmVO> alarmList = new ArrayList<AlarmVO>();
					for (int i = 0; i < dataList1.size(); i++) {
						JSONObject alarmData = (JSONObject) dataList1.get(i);
						AlarmVO alarmVO = new AlarmVO();
						alarmVO.setAlarmCode(alarmData.get("alarmCode").toString());
						alarmVO.setAlarmMsg(alarmData.get("alarmMsg").toString());
						alarmVO.setDvcId(rareVO.getDvcId());
						alarmList.add(alarmVO);
					}

					for (int n = 0; n < alarmList.size(); n++) {
						for (int m = 0; m < exceptionList.size(); m++) {
							if (alarmList.get(n).getAlarmCode().equals(exceptionList.get(m).getAlarmCode())) {
								alarmCheck++;
							} else {

							}
						}
					}

					if (alarmCheck != alarmList.size()) {
						rareVO.setChartStatus("ALARM");
					} else if (rareVO.getPmcCut() == 1) {
						rareVO.setChartStatus("CUT");
					} else if (rareVO.getPmcStl() == 1) {
						rareVO.setChartStatus("IN-CYCLE");
					} else {
						rareVO.setChartStatus("WAIT");
					}
				} else if (rareVO.getPmcCut() == 1) {
					rareVO.setChartStatus("CUT");
				} else if (rareVO.getPmcStl() == 1) {
					rareVO.setChartStatus("IN-CYCLE");
				} else {
					rareVO.setChartStatus("WAIT");
				}

				Date newDate = sdf.parse(rareVO.getStartDateTime());
				Calendar newCal = Calendar.getInstance();
				newCal.setTime(newDate);

				if (newCal.after(beforeCal)) {
					rareDataList.add(rareVO);
				} else if (!check) {
					rareDataList.add(rareVO);
				}

			}

			if (rareDataList.size() > 0) {
				String getDate = rareDataList.get(rareDataList.size() - 1).getStartDateTime();

				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date d = simpleDateFormat.parse(getDate);
				long milliseconds = d.getTime();
				/*
				 * if(System.currentTimeMillis()-milliseconds<60000) {
				 * sqlSessionTemplate.insert(namespace + "updateLastData",
				 * rareDataList.get(rareDataList.size() - 1)); }
				 */

				List<AlarmVO> prevAlarmList = new ArrayList<AlarmVO>();
				prevAlarmList = sqlSessionTemplate.selectList(namespace + "getLastAlarmList",
						rareDataList.get(0).getDvcId());

				if (prevAlarmList.size() > 0) {
					sqlSessionTemplate.delete(namespace + "deleteLastAlarmList", prevAlarmList);
				}

				long start1 = System.currentTimeMillis(); // 시작하는 시점 계산
				for (int i = 0; i < rareDataList.size(); i++) {
					String start_date_time = rareDataList.get(i).getStartDateTime();
					JSONParser jsonParser1 = new JSONParser();
					JSONArray dataList1 = (JSONArray) jsonParser1.parse(rareDataList.get(i).getAlarm());
					List<AlarmVO> nowAlarmList = new ArrayList<AlarmVO>();
					List<AlarmVO> insertList = new ArrayList<AlarmVO>();
					for (int j = 0; j < dataList1.size(); j++) {
						JSONObject alarmData = (JSONObject) dataList1.get(j);
						AlarmVO alarmVO = new AlarmVO();
						alarmVO.setAlarmCode(alarmData.get("alarmCode").toString());
						alarmVO.setAlarmMsg(alarmData.get("alarmMsg").toString());
						alarmVO.setStart_date_time(rareDataList.get(i).getStartDateTime());
						alarmVO.setDvcId(rareDataList.get(i).getDvcId());
						nowAlarmList.add(alarmVO);
					}

					if (prevAlarmList.size() == 0) {
						for (int n = 0; n < nowAlarmList.size(); n++) {
							prevAlarmList.add(nowAlarmList.get(n));
						}
					}

					for (int n = 0; n < prevAlarmList.size(); n++) {
						boolean checker = false;
						for (int m = 0; m < nowAlarmList.size(); m++) {
							if (prevAlarmList.get(n).getAlarmCode().equals(nowAlarmList.get(m).getAlarmCode())
									&& prevAlarmList.get(n).getEnd_date_time() == null) {
								checker = true;
							}
						}
						if (!checker) {
							if (prevAlarmList.get(n).getEnd_date_time() == null) {
								prevAlarmList.get(n).setEnd_date_time(start_date_time);
							}
						}
					}

					for (int n = 0; n < nowAlarmList.size(); n++) {
						boolean checker = false;
						for (int m = 0; m < prevAlarmList.size(); m++) {
							if (nowAlarmList.get(n).getAlarmCode().equals(prevAlarmList.get(m).getAlarmCode())
									&& prevAlarmList.get(m).getEnd_date_time() == null) {
								checker = true;
							}
						}

						if (!checker) {
							prevAlarmList.add(nowAlarmList.get(n));
						}
					}
				}

				if (prevAlarmList.size() > 0) {
					sqlSessionTemplate.insert(namespace + "insertAlarmList", prevAlarmList);
				}

				Collections.sort(rareDataList, new Comparator<RareVO>() {

					@Override
					public int compare(RareVO o1, RareVO o2) {
						// TODO 자동 생성된 메소드 스텁
						return o1.getStartDateTime().compareTo(o2.getStartDateTime());
					}

				});

				for (int i = 0; i < rareDataList.size(); i++) {
					if (i < rareDataList.size() && i + 1 < rareDataList.size()) {
						rareDataList.get(i).setEndDateTime(rareDataList.get(i + 1).getStartDateTime());
					}
					// System.out.println(rareDataList.get(i));
				}
				rareDataList.get(rareDataList.size() - 1).setEndDateTime(null);

				sqlSessionTemplate.update(namespace + "updatePrevRareEndTime", rareDataList.get(0));

				sqlSessionTemplate.insert(namespace + "setRareData", rareDataList);
				sqlSessionTemplate.update(namespace + "updateLastRareData", rareDataList.get(rareDataList.size() - 1));
				// long end1 = System.currentTimeMillis(); // 시작하는 시점 계산
				// System.out.println("나머지 로직에 걸린시간 : " + (end1 - start1)); // 실행 시간 계산 및 출력

			} else {
				RareVO newRareVO = new RareVO();
				newRareVO.setDvcId(Integer.parseInt(dvcId));
				sqlSessionTemplate.insert(namespace + "updateLastDataOnlyTime", newRareVO.getDvcId());
			}

			result = "success";
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Rare Data insert is failed. with " + inputStrJson);
			result = "failed";
		}

		// long Wholeend = System.currentTimeMillis(); // 프로그램이 끝나는 시점 계산
		// System.out.println("로직의 총 시간 : " + (Wholeend - Wholestart));

		return result;
	}

	/*
	 * (비Javadoc)
	 * 
	 * @see
	 * com.unomic.dulink.adapter.service.DataService#setLampData(java.lang.String)
	 */
	@Override
	public String setLampData(String val) throws Exception {
		// TODO Auto-generated method stub
		String str = "";
		try {
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
			JSONArray jsonArray = (JSONArray) jsonObj.get("data");
			List<LampVO> list = new ArrayList<LampVO>();
			List<LampVO> valueList = sqlSessionTemplate.selectList(namespace + "getLampExpression");

			for (int i = 0; i < jsonArray.size(); i++) {
				JSONObject tempObj = (JSONObject) jsonArray.get(i);

				LampVO device = new LampVO();
				device.setDvcId(Integer.parseInt(tempObj.get("dvcId").toString()));
				device.setAddressValue(tempObj.get("addressValue").toString());

				list.add(device);
			}

			String[] valueString = getContact(String.valueOf(list.get(0).getDvcId())).split(",");
			List<String> storeValue = new ArrayList<String>();
			List<String> inputValue = new ArrayList<String>();

			JSONParser parse = new JSONParser();
			Object obj = parse.parse(list.get(0).getAddressValue());
			JSONObject jsonObject = (JSONObject) obj;
			Set key = jsonObject.keySet();

			Iterator<String> iter = key.iterator();

			while (iter.hasNext()) {
				String keyname = iter.next();
				inputValue.add(keyname);
			}

			for (int i = 0; i < valueString.length; i++) {
				storeValue.add(valueString[i]);
			}

			int size = 0;

			if (storeValue.size() != inputValue.size()) {
				return "changed";
			} else {
				for (int i = 0; i < storeValue.size(); i++) {
					for (int j = 0; j < inputValue.size(); j++) {
						if (storeValue.get(i).equals(inputValue.get(j))) {
							size++;
						}
					}
				}
			}

			if (size == storeValue.size()) {
			} else {
				return "changed";
			}

			for (int i = 0; i < list.size(); i++) {
				for (int j = 0; j < valueList.size(); j++) {
					if (list.get(i).getDvcId() == valueList.get(j).getDvcId()) {
						if (valueList.get(j).getRedExpression() != null
								&& valueList.get(j).getRedExpression().length() != 0) {
							if (checkVaiable(list.get(i).getAddressValue(),
									valueList.get(j).getRedExpression()) == 60) {
								return "NO-CONNECTION";
							}
							list.get(i).setRed(
									checkVaiable(list.get(i).getAddressValue(), valueList.get(j).getRedExpression()));
						}
						if (valueList.get(j).getRedBlinkExpression() != null
								&& valueList.get(j).getRedBlinkExpression().length() != 0) {
							list.get(i).setRedBlink(checkVaiable(list.get(i).getAddressValue(),
									valueList.get(j).getRedBlinkExpression()));
						}
						if (valueList.get(j).getYellowExpression() != null
								&& valueList.get(j).getYellowExpression().length() != 0) {
							list.get(i).setYellow(checkVaiable(list.get(i).getAddressValue(),
									valueList.get(j).getYellowExpression()));
						}
						if (valueList.get(j).getYellowBlinkExpression() != null
								&& valueList.get(j).getYellowBlinkExpression().length() != 0) {
							list.get(i).setYellowBlink(checkVaiable(list.get(i).getAddressValue(),
									valueList.get(j).getYellowBlinkExpression()));
						}
						if (valueList.get(j).getGreenExpression() != null
								&& valueList.get(j).getGreenExpression().length() != 0) {
							list.get(i).setGreen(
									checkVaiable(list.get(i).getAddressValue(), valueList.get(j).getGreenExpression()));
						}
						if (valueList.get(j).getGreenBlinkExpression() != null
								&& valueList.get(j).getGreenBlinkExpression().length() != 0) {
							list.get(i).setGreenBlink(checkVaiable(list.get(i).getAddressValue(),
									valueList.get(j).getGreenBlinkExpression()));
						}
					}
				}
			}

			List<LampVO> dvcList = sqlSessionTemplate.selectList(namespace + "getDeviceName", list);

			for (int i = 0; i < list.size(); i++) {
				for (int j = 0; j < dvcList.size(); j++) {
					if (list.get(i).getDvcId() == dvcList.get(j).getDvcId()) {
						list.get(i).setName(dvcList.get(j).getName());
					}
				}
			}

			int dvcId = list.get(0).getDvcId();

			int count = (int) sqlSessionTemplate.selectOne(namespace + "getLampCount", dvcId);
			if (count > 0) {
				sqlSessionTemplate.update(namespace + "updateLampStatus", list.get(0));
			} else {
				sqlSessionTemplate.insert(namespace + "insertLampStatus", list);
			}
			LampVO latestData = (LampVO) sqlSessionTemplate.selectOne(namespace + "getLatestLampData", list.get(0));

			// 2019.10.10 wilson
			// green > red > yellow   =>  red > green > yellow
			
			if (list.get(0).getRed() == 1) {
				list.get(0).setChartStatus("red");
			} else if (list.get(0).getRedBlink() == 1) {
				list.get(0).setChartStatus("redBlink");
			} else if (list.get(0).getGreen() == 1) {
				list.get(0).setChartStatus("green");
			} else if (list.get(0).getGreenBlink() == 1) {
				list.get(0).setChartStatus("greenBlink");
			} else if (list.get(0).getYellow() == 1) {
				list.get(0).setChartStatus("yellow");
			} else if (list.get(0).getYellowBlink() == 1) {
				list.get(0).setChartStatus("yellowBlink");
			} else {
				list.get(0).setChartStatus("gray");
			}
			
			
			/*System.out.println("Lamp Data");
			System.out.println(list.get(0));
			System.out.println(latestData);*/
			if (count > 0) {
				try {
					if (list.get(0).getRed() != latestData.getRed()
							|| list.get(0).getRedBlink() != latestData.getRedBlink()
							|| list.get(0).getGreen() != latestData.getGreen()
							|| list.get(0).getGreenBlink() != latestData.getGreenBlink()
							|| list.get(0).getYellow() != latestData.getYellow()
							|| list.get(0).getYellowBlink() != latestData.getYellowBlink()) {
						sqlSessionTemplate.update(namespace + "updateLampData", list.get(0));
						sqlSessionTemplate.insert(namespace + "insertLampData", list.get(0));
					}
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
			} else {
				sqlSessionTemplate.insert(namespace + "insertLampData", list.get(0));
			}

			str = "success";
		} catch (Exception e) {
			str = "failed";
			System.out.println("Lamp Data insert is failed. with " + val);
			e.printStackTrace();
			// TODO: handle exception
		}

		return str;
	}

	/**
	 * 모든 램프 표현에 대하여 접점 수집 후 중복 제거 그것의 결과를 String 으로 전송. 수신자 측에서는 해당 response 결과를
	 * String를 으로 받아 ',' 로 split 해서 사용하면됨.
	 *
	 * @author John.Joe
	 * @param dvcId
	 *            the dvc id
	 * @return the contact
	 * @throws Exception
	 *             the exception
	 * @see com.unomic.service.LampService#getContact(java.lang.String)
	 * @since 2018-02-05
	 */
	@Override
	public String getContact(String dvcId) throws Exception {
		LampVO lampExpression = (LampVO) sqlSessionTemplate.selectOne(namespace + "getLampData", dvcId);

		String pattern = "^[ADEFGKRXYZ][0-9]+[.][0-9]";
		String notPattern = "^[!][ADEFGKRXYZ][0-9]+[.][0-9]";

		List<String> contactList = new ArrayList<String>();

		if (lampExpression.getRedExpression() != null) {
			String[] split = lampExpression.getRedExpression().split(" ");
			for (int i = 0; i < split.length; i++) {
				if (split[i].trim().matches(pattern)) {
					contactList.add(split[i].trim());
				} else if (split[i].matches(notPattern)) {
					contactList.add(split[i].trim().substring(1));
				}
			}
		}
		if (lampExpression.getRedBlinkExpression() != null) {
			String[] split = lampExpression.getRedBlinkExpression().split(" ");
			for (int i = 0; i < split.length; i++) {
				if (split[i].trim().matches(pattern)) {
					contactList.add(split[i].trim());
				} else if (split[i].trim().matches(notPattern)) {
					contactList.add(split[i].trim().substring(1));
				}
			}
		}
		if (lampExpression.getYellowExpression() != null) {
			String[] split = lampExpression.getYellowExpression().split(" ");
			for (int i = 0; i < split.length; i++) {
				if (split[i].trim().matches(pattern)) {
					contactList.add(split[i].trim());
				} else if (split[i].trim().matches(notPattern)) {
					contactList.add(split[i].trim().substring(1));
				}
			}
		}
		if (lampExpression.getYellowBlinkExpression() != null) {
			String[] split = lampExpression.getYellowBlinkExpression().split(" ");
			for (int i = 0; i < split.length; i++) {
				if (split[i].trim().matches(pattern)) {
					contactList.add(split[i].trim());
				} else if (split[i].trim().matches(notPattern)) {
					contactList.add(split[i].trim().substring(1));
				}
			}
		}
		if (lampExpression.getGreenExpression() != null) {
			String[] split = lampExpression.getGreenExpression().split(" ");
			for (int i = 0; i < split.length; i++) {
				if (split[i].trim().matches(pattern)) {
					contactList.add(split[i].trim());
				} else if (split[i].trim().matches(notPattern)) {
					contactList.add(split[i].trim().substring(1));
				}
			}
		}
		if (lampExpression.getGreenBlinkExpression() != null) {
			String[] split = lampExpression.getGreenBlinkExpression().split(" ");
			for (int i = 0; i < split.length; i++) {
				if (split[i].trim().matches(pattern)) {
					contactList.add(split[i].trim());
				} else if (split[i].trim().matches(notPattern)) {
					contactList.add(split[i].trim().substring(1));
				}
			}
		}

		List<String> resultList = new ArrayList<String>();

		resultList = new ArrayList<String>();
		for (int i = 0; i < contactList.size(); i++) {
			if (!resultList.contains(contactList.get(i))) {
				resultList.add(contactList.get(i));
			}
		}

		// System.out.println("adtId : " + dvcId + ", 접점 : " + resultList);// 중복제거가 잘
		// 되었는지 확인
		String contact = "";
		for (int i = 0; i < resultList.size(); i++) {
			if (i == resultList.size() - 1) {
				contact += resultList.get(i);
			} else {
				contact += resultList.get(i) + ",";
			}
		}

		return contact;
	}

	/**
	 * @see com.unomic.dulink.adapter.service.DataService#setFrequentData(java.lang.String)
	 * @author John.Joe
	 * @since 2018-06-18 end_date_time 에 대하여 업데이트 및 데이터 중복, 비정확 update 제거
	 * 
	 */
	@Override
	public String setFrequentData(String strJson) throws Exception {
		long Wholestart = System.currentTimeMillis(); // 시작하는 시점 계산
		String result = "";
		List<FrequentVO> list = new ArrayList<FrequentVO>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// System.out.println(strJson);
		try {

			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = new JSONObject();

			jsonObject = (JSONObject) jsonParser.parse(strJson);
			JSONArray dataList = (JSONArray) jsonObject.get("data");
			String dvcId = jsonObject.get("dvcId").toString();

			String lastStart = (String) sqlSessionTemplate.selectOne(namespace + "getLastFreqentData", dvcId);
			//System.out.println(lastStart);
			Date beforeDate = new Date();
			Calendar beforeCal = Calendar.getInstance();
			if (lastStart != null) {
				beforeDate = sdf.parse(lastStart);
				beforeCal = Calendar.getInstance();
				beforeCal.setTime(beforeDate);
			}
			
			for (int j = 0; j < dataList.size(); j++) {
				JSONObject frequentData = (JSONObject) dataList.get(j);
				FrequentVO frequentVO = new FrequentVO();
				frequentVO.setDvcId(Integer.parseInt(dvcId));
				frequentVO.setStart_date_time(frequentData.get("start_date_time").toString());
				frequentVO.setSpd_ld(Float.parseFloat(frequentData.get("spd_ld").toString()));
				frequentVO.setSpd_act_speed(Integer.parseInt(frequentData.get("spd_act_speed").toString()));
				frequentVO.setAct_fd(Integer.parseInt(frequentData.get("act_fd").toString()));
				frequentVO.setAxis_ld_z(Float.parseFloat(frequentData.get("axis_ld_z").toString()));

				Date newDate = sdf.parse(frequentVO.getStart_date_time());
				Calendar newCal = Calendar.getInstance();
				newCal.setTime(newDate);

				if (newCal.after(beforeCal) || lastStart==null) {
					list.add(frequentVO);
				}

			}

			if (list.size() > 0) {
				Collections.sort(list, new Comparator<FrequentVO>() {

					@Override
					public int compare(FrequentVO o1, FrequentVO o2) {
						// TODO 자동 생성된 메소드 스텁
						return o1.getStart_date_time().compareTo(o2.getStart_date_time());
					}

				});

				for (int i = 0; i < list.size(); i++) {
					if (i < list.size() && i + 1 < list.size()) {
						list.get(i).setEnd_date_time(list.get(i + 1).getStart_date_time());
					}
					// System.out.println(list.get(i));
				}
				list.get(list.size() - 1).setEnd_date_time(null);
				lastStart = (String) sqlSessionTemplate.selectOne(namespace + "getLastFreqentData", dvcId);
				Date newDate = sdf.parse(list.get(0).getStart_date_time());
				Calendar newCal = Calendar.getInstance();
				newCal.setTime(newDate);
				
				if (newCal.after(beforeCal) || lastStart==null) {
					sqlSessionTemplate.update(namespace + "updateEndFrequentData", list.get(0));
					
					//System.out.println(list);
					
					sqlSessionTemplate.insert(namespace + "insertFrequentData", list);
				}else {
					//System.out.println("NOT INSERT DATA");
				}
				
			}else {
				//System.out.println(lastStart);
				//System.out.println("넣을 데이터 없음");
				sqlSessionTemplate.insert(namespace + "updateLastDataOnlyTime", dvcId);
			}

			result = "success";
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Frequently Data insert failed. with" + strJson);
			result = "failed";
		}
		long Wholeend = System.currentTimeMillis(); // 프로그램이 끝나는 시점 계산
		//System.out.println("로직의 총 시간 : " + (Wholeend - Wholestart));
		return result;
	}

	/**
	 * Check vaiable.
	 *
	 * @param value
	 *            the value
	 * @param expression
	 *            the expression
	 * @return the int
	 * @throws Exception
	 *             the exception
	 */
	public int checkVaiable(String value, String expression) throws Exception {

		String split[] = expression.split(" ");

		String pattern = "^[ADEFGKRXY][0-9]+[.][0-9]";
		String notPattern = "^[!][ADEFGKRXY][0-9]+[.][0-9]";

		ScriptEngineManager mgr = new ScriptEngineManager();
		ScriptEngine engine = mgr.getEngineByName("JavaScript");
		// System.out.println("====================================================================");
		// System.out.println("expression : " + expression);
		// System.out.println("value : " + value);
		JSONParser parse = new JSONParser();
		Object obj = parse.parse(value);

		JSONObject jsonObj = (JSONObject) obj;
		// System.out.println("키의 갯수 : " + jsonObj.size());

		Set key = jsonObj.keySet();

		Iterator<String> iter = key.iterator();
		HashMap map = new HashMap();

		boolean checker = false;
		while (iter.hasNext()) {
			String keyname = iter.next();
			// System.out.println("key : " + keyname.toUpperCase() + " 값 : " +
			// jsonObj.get(keyname));
			if (Integer.parseInt(jsonObj.get(keyname).toString()) == -2) {
				checker = true;
				break;
			}
			map.put(keyname.toUpperCase(), jsonObj.get(keyname));
		}

		if (checker) {
			return 60;
		}

		for (int i = 0; i < split.length; i++) {
			if (split[i].trim().equals("(") || split[i].trim().equals(")") || split[i].trim().equals("|")
					|| split[i].trim().equals("&") || split[i].trim().equals("!")) {
				// System.out.println("구문 상수 : " + split[i]);
			} else if (split[i].trim().matches(pattern)) {
				// System.out.println("구문 변수 : " + split[i]);
				split[i] = String.valueOf(map.get(split[i].trim()));
			} else if (split[i].trim().matches(notPattern)) {
				// System.out.println("구문 변수 : " + split[i].trim());
				split[i] = split[i].substring(split[i].indexOf("!") + 1);
				// System.out.println("변환 변수 : " + split[i]);
				split[i] = String.valueOf(map.get(split[i].trim()));
				if (Integer.parseInt(split[i]) == 1) {
					split[i] = "0";
				} else if (Integer.parseInt(split[i]) == 0) {
					split[i] = "1";
				}
				// System.out.println("변환된 변수의 값 : " + split[i]);
			}
		}

		String caculator = "";

		for (int i = 0; i < split.length; i++) {
			caculator += split[i];
		}

		// System.out.println("결과 : " + engine.eval(caculator));

		int result = 0;
		try {
			result = (int) engine.eval(caculator);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String setPushToken(TokenVO tokenVo) throws Exception {
		String result = "failed";
		try {
			sqlSessionTemplate.insert(namespace + "setPushReg", tokenVo);
			sqlSessionTemplate.insert(namespace + "setPushToken", tokenVo);			
			result = "success";
		} catch (Exception e) {
			e.printStackTrace();

		}
		return result;
	}

	@Override
	public String getPushToken(TokenVO token) throws Exception {
		int count = (int) sqlSessionTemplate.selectOne(namespace + "getPushToken", token);

		if (count > 0) {
			return "change";
		} else {
			sqlSessionTemplate.insert(namespace + "setPushToken", token);
			return "success";
		}
	}

}
