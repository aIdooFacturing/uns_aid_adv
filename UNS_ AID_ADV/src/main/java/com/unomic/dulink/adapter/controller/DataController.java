package com.unomic.dulink.adapter.controller;

import java.io.File;
import java.text.SimpleDateFormat;
//import java.time.ZoneOffset;
//import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.core.JsonParser;
import com.unomic.dulink.adapter.domain.TokenVO;
import com.unomic.dulink.adapter.service.DataService;

@Controller
@RequestMapping("/adapter")
public class DataController {

	@Autowired
	DataService adapterService;
	
	

	@RequestMapping("/setRareData")
	@ResponseBody
	public String setRareData(@RequestBody String strJson) {
		String result = "";
		try {
			result = adapterService.setRareData(strJson);
		} catch (Exception e) {
			result = "fail";
		}
		return result;
	}

	@RequestMapping("/setLampData")
	@ResponseBody
	public String setLampData(@RequestBody String strJson) {
		String str = "";
		try {
			str = adapterService.setLampData(strJson);
		} catch (Exception e) {
			str = "fail";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping("/getLampData")
	@ResponseBody
	public String getContact(String dvcId) {
		String str = "";
		try {
			str = adapterService.getContact(dvcId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}

	@RequestMapping("/setFrequentData")
	@ResponseBody
	public String setFrequentData(@RequestBody String strJson) {
		String str = "";
		try {
			str = adapterService.setFrequentData(strJson);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}

	@RequestMapping("/getInitTime")
	@ResponseBody
	public String getInitTime() {
		
		String str = "";
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		str = dayTime.format(cal.getTime());

		return str;
	}
	
	@RequestMapping("/setPushToken")
	@ResponseBody
	public String setPushToken(TokenVO token) {
		String str = "";
		try {
			// System.out.println(token);
			str = adapterService.setPushToken(token);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}
	
	@RequestMapping("/getPushToken")
	@ResponseBody
	public String getPushToken(TokenVO token) {
		String str = "";
		try {
			str = adapterService.getPushToken(token);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}
	
	@RequestMapping("/checkJWT")
	public void checkJWT() {
		
		try {
			/*
			DULink 측 코드
			*/
			List<String> objectList = new ArrayList<String>();
			
			for(int i=0;i<2;i++) {
				JSONObject sub_object = new JSONObject();
				sub_object.put("mode", "MEM");
				sub_object.put("status", "START");
				sub_object.put("main_prg_name", "O2000");
				sub_object.put("mdl_m1", "1");
				sub_object.put("spd_ovrd", "0");
				sub_object.put("alarm", "0");
				
				objectList.add(sub_object.toString());
			}
			
			
			// unomic을 키로 하는 HMAC256 암호생성 알고리즘 생성
			Algorithm algorithm = Algorithm.HMAC256("unomic");
			
			
			//토큰 생성
			// withClaim 함수는 배열이 아닌것을 생성
			// withArrayClaim 는 배열인것을 생성 ( 반드시 String[] 의 형태로만 입력가능 )
			String token = JWT.create().withClaim("dvcId", 600).withClaim("send_date_time","2018-07-19 13:12:05").withArrayClaim("data", objectList.toArray(new String[objectList.size()])).sign(algorithm);
			
			
			//위에서 생성된 token을 보내면 됨
			System.out.println("token : " + token);
			
			/*
			DULink 측 코드 끝
			*/
			
			
			
			/*
			서버측 코드
			위에서 생성된 암호화된 JSON 코드가 잘 파싱이 되는지 확인
			 */
	        System.out.println("------------ Decode JWT ------------");
	        String[] split_string = token.split("\\.");
	        String base64EncodedHeader = split_string[0];
	        String base64EncodedBody = split_string[1];

	        System.out.println("~~~~~~~~~ JWT Header ~~~~~~~");
	        Base64 base64Url = new Base64(true);
	        String header = new String(base64Url.decode(base64EncodedHeader));
	        System.out.println("JWT Header : " + header);


	        System.out.println("~~~~~~~~~ JWT Body ~~~~~~~");
	        String body = new String(base64Url.decode(base64EncodedBody)).replace("\\", "");
	        body = body.replace("\"{", "{");
	        body = body.replace("}\"", "}");
	        System.out.println("JWT Body : "+body);    
	        
	        System.out.println("------------------------------------");
	        
	        
	        JSONParser jsonParser = new JSONParser();
	        JSONObject jsonObject = new JSONObject();

	        jsonObject = (JSONObject) jsonParser.parse(body);
	        
	        
	    	String dvcId = jsonObject.get("dvcId").toString();
	    	
	    	System.out.println("dvcId : " + dvcId);
	    	
	    	JSONArray dataList = (JSONArray) jsonObject.get("data");
	    	
	    	for(int i=0;i<dataList.size();i++) {
	    		JSONObject rareData = (JSONObject) dataList.get(i);
	    		
	    		System.out.println("mode : "+rareData.get("mode").toString());
	    		System.out.println("spd_ovrd : " + rareData.get("spd_ovrd").toString());
	    		System.out.println("alarm : "+rareData.get("alarm").toString());
	    		System.out.println("main_prg_name : " + rareData.get("main_prg_name").toString());
	    		System.out.println("status : " + rareData.get("status").toString());
	    		
	    	}
	    	
	    	/*
			서버측 코드끝
			 */
	    	
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
