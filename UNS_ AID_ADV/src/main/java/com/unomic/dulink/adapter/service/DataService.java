package com.unomic.dulink.adapter.service;

import com.unomic.dulink.adapter.domain.TokenVO;

public interface DataService {

	String setRareData(String strJson) throws Exception;

	String getContact(String strJson) throws Exception;

	String setLampData(String val) throws Exception;

	String setFrequentData(String strJson)throws Exception;

	String setPushToken(TokenVO tokenVO) throws Exception;

	String getPushToken(TokenVO token) throws Exception;

}
