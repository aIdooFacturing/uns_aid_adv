package com.unomic.dulink.adapter.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RareVO {

	int dvcId;
	int idx;
	String startDateTime;
	String endDateTime;
	String chartStatus;
	String status;
	int pmcStl;
	int pmcCut;
	int pmcSingleBlock;
	int pmcFeedHold;
	int pmcEmg;
	String cncInfo;
	String mode;
	String mainPrgmName;
	String prgmHead;
	String mdlM1;
	String mdlM2;
	String mdlM3;
	String mdlT;
	String mdlH;
	String mdlD;
	String alarm;
	int partCount;
	int ttlPartCount;
	int spdOvrd;
	int fdOvrd;
	
}
